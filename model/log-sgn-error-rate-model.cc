/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Sian Jin <sianjin@uw.edu>
 *          Sébastien Deronne <sebastien.deronne@gmail.com>
 */

#include <math.h>
#include <complex.h>
#include "ns3/log.h"
#include "ns3/enum.h"
#include "ns3/double.h"
#include "ns3/random-variable-stream.h"
#include "ns3/table-based-error-rate-model.h"
#include "ns3/wifi-tx-vector.h"
#include "ns3/wifi-utils.h"
#include "log-sgn-error-rate-model.h"
#include "log-sgn-parameters.h"

namespace ns3 {

static const double LOG_SGN_PRECISION = 1e-3;

NS_OBJECT_ENSURE_REGISTERED (LogSgnErrorRateModel);

NS_LOG_COMPONENT_DEFINE ("LogSgnErrorRateModel");

TypeId
LogSgnErrorRateModel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::LogSgnErrorRateModel")
    .SetParent<ErrorRateModel> ()
    .SetGroupName ("WifiLogSgn")
    .AddConstructor<LogSgnErrorRateModel> ()
    .AddAttribute ("ChannelModelType",
                   "The channel model type to use",
                   EnumValue (LogSgnErrorRateModel::CHANNEL_MODEL_D),
                   MakeEnumAccessor (&LogSgnErrorRateModel::m_channelModelType),
                   MakeEnumChecker ( LogSgnErrorRateModel::CHANNEL_MODEL_D, "ChannelModelD"))
  ;
  return tid;
}

LogSgnErrorRateModel::LogSgnErrorRateModel ()
{
  NS_LOG_FUNCTION (this);

  m_normRandomVariable0 = CreateObject<NormalRandomVariable> ();
  m_normRandomVariable1 = CreateObject<NormalRandomVariable> ();

  m_uniformRandomVariable = CreateObject<UniformRandomVariable> ();
  m_uniformRandomVariable->SetAttribute ("Min", DoubleValue (0.0));
  m_uniformRandomVariable->SetAttribute ("Max", DoubleValue (1.0));

  m_table = CreateObject<TableBasedErrorRateModel> ();
}

LogSgnErrorRateModel::~LogSgnErrorRateModel ()
{
  NS_LOG_FUNCTION (this);
  m_table = 0;
}

double
LogSgnErrorRateModel::DoGetChunkSuccessRate (WifiMode mode, const WifiTxVector& txVector, double snr, uint64_t nbits,
                                             uint8_t numRxAntennas, WifiPpduField field, uint16_t staId) const
{
  NS_LOG_FUNCTION (this << mode << txVector << snr << nbits << +numRxAntennas << field << staId);

  double per;
  NS_ASSERT (field > WIFI_PPDU_FIELD_PREAMBLE);
  if (field == WIFI_PPDU_FIELD_DATA)
    {
      uint16_t bw = 20;
      uint8_t nss = 1;
      uint8_t muMimoUsers = 0;
      if (txVector.IsMu () && (staId != SU_STA_ID))
        {
          NS_ASSERT (mode == txVector.GetMode (staId));
          bw = HeRu::GetBandwidth (txVector.GetRu (staId).GetRuType ());
          nss = txVector.GetNss (staId);
        }
      else if (!txVector.IsMu ())
        {
          bw = txVector.GetChannelWidth ();
          nss = txVector.GetNss ();
        }

      auto mcs = m_table->GetMcsForMode (mode);
      NS_ABORT_MSG_UNLESS (mcs.has_value (), "No MCS found for WifiMode " << mode);
      double effectiveSnr = GetEffectiveSnr (RatioToDb (snr), mcs.value (), bw,
                                             nss, txVector.GetNTx (), numRxAntennas, muMimoUsers);

      per = 1.0 - m_table->GetChunkSuccessRate (mode, txVector, effectiveSnr, nbits, numRxAntennas, field, staId);
    }
  else if (field == WIFI_PPDU_FIELD_NON_HT_HEADER)
    {
      per = 1.0 - m_table->GetChunkSuccessRate (mode, txVector, snr, nbits, numRxAntennas, field, staId);
    }
  else
    {
      // This error model assumes that HT-SIG, VHT-SIG or HE-SIG are
      // perfectly received, due to a lack of support in link simulations
      per = 0.0;
    }

  if (per < LOG_SGN_PRECISION)
    {
      per = 0.0;
    }
  return 1.0 - per;
}

double
LogSgnErrorRateModel::GetEffectiveSnr (double snr, uint8_t mcs, uint16_t width, uint8_t nss, uint8_t nTx, uint8_t nRx, uint8_t muMimoUsers) const
{
  NS_LOG_FUNCTION (this << snr << +mcs << width << +nss << +nTx << +nRx << +muMimoUsers);

  if (m_channelModelType != CHANNEL_MODEL_D)
    {
      NS_FATAL_ERROR ("Only channel model D is supported by the Log-SGN model");
      return 0;
    }

  std::size_t bwIndex;
  switch (width)
    {
      case 2:
        bwIndex = 0;
        break;
      case 4:
        bwIndex = 1;
        break;
      case 8:
        bwIndex = 2;
        break;
      case 20:
        bwIndex = 3;
        break;
      case 40:
        bwIndex = 4;
        break;
      case 80:
        bwIndex = 5;
        break;
      case 160:
        bwIndex = 6;
        break;
      default:
        NS_FATAL_ERROR (width << " MHz is not supported by the Log-SGN model");
        return 0;
    }

  std::map<double, LogSgnParameters, CompareLogSgnKeys> parameterMap;
  if (m_channelModelType == CHANNEL_MODEL_D)
    {
      parameterMap = Channel_D_Siso_Log_Sgn_Parameters_Map [mcs][bwIndex];
    }

  if (parameterMap.empty ())
    {
      NS_FATAL_ERROR ("Combination MCS-" << +mcs << " and channel width " << width << " MHz is not supported by the Log-SGN model for configuration " << +nTx << "x" << +nRx << ":" << +nss);
      return 0;
    }

  double delta = 0;
  LogSgnParameters parameters;
  auto it = parameterMap.find (snr);
  if (it == parameterMap.end ())
    {
      if (snr < parameterMap.begin ()->first)
        {
          delta = snr - parameterMap.begin ()->first;
          parameters = parameterMap.begin ()->second;
        }
      else if (snr > parameterMap.rbegin ()->first)
        {
          delta = snr - parameterMap.rbegin ()->first;
          parameters = parameterMap.rbegin ()->second;
        }
      else
        {
          auto upper = parameterMap.upper_bound (snr);
          auto lower = upper;
          if (lower != parameterMap.begin ())
            {
              lower--;
            }

          double prob = (snr - lower->first) / (upper->first - lower->first);
          double uniformRv = m_uniformRandomVariable->GetValue ();
          if (uniformRv > prob)
            {
              parameters = lower->second;
            }
          else
            {
              parameters = upper->second;
            }
        }
    }
  else
    {
      parameters = it->second;
    }

  double normRv0 = m_normRandomVariable0->GetValue (0.0, 1.0);
  double alpha = std::sqrt (parameters.m_lambda2) * normRv0 + parameters.m_lambda1;
  double alphaPow2 = std::pow (alpha, 2.0);
  double meanNormRandomVariableUnscale = std::sqrt ((1.0 + alphaPow2) / 2.0);
  double meanNormRandomVariable = meanNormRandomVariableUnscale * parameters.m_mu;
  double stdNormRandomVariable = parameters.m_sigma;
  double varNormRandomVariable = std::pow (stdNormRandomVariable, 2.0);
  double normRv1 = m_normRandomVariable1->GetValue (meanNormRandomVariable, varNormRandomVariable);
  double normRv2 = m_normRandomVariable1->GetValue (meanNormRandomVariable, varNormRandomVariable);
  double maxTwoNormalRv = std::max (normRv1, normRv2);
  double minTwoNormalRv = std::min (normRv1, normRv2);
  double sgnRv = (((1.0 + alpha) / std::sqrt (2.0 * (1.0 + alphaPow2))) * maxTwoNormalRv) + (((1.0 - alpha) / std::sqrt (2.0 * (1.0 + alphaPow2))) * minTwoNormalRv);
  double effectiveSnr = std::exp (sgnRv);

  return DbToRatio (RatioToDb (effectiveSnr) + delta);
}

bool
LogSgnErrorRateModel::IsAwgn (void) const
{
  return false;
}

int64_t
LogSgnErrorRateModel::AssignStreams (int64_t stream)
{
  NS_LOG_FUNCTION (this << stream);
  int64_t currentStream = stream;
  m_normRandomVariable0->SetStream (currentStream++);
  m_normRandomVariable1->SetStream (currentStream++);
  m_uniformRandomVariable->SetStream (currentStream++);
  return (currentStream - stream);
}

} //namespace ns3
