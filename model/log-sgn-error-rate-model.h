/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Sian Jin <sianjin@uw.edu>
 *          Sébastien Deronne <sebastien.deronne@gmail.com>
 */

#ifndef LOG_SGN_ERROR_RATE_MODEL_H
#define LOG_SGN_ERROR_RATE_MODEL_H

#include "ns3/error-rate-model.h"
#include "ns3/wifi-mode.h"

namespace ns3 {

class TableBasedErrorRateModel;
class NormalRandomVariable;
class UniformRandomVariable;

/**
 * \ingroup wifi-l2s-error-models
 * \brief the interface for the LOG-SGN error model
 */
class LogSgnErrorRateModel : public ErrorRateModel
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);

  LogSgnErrorRateModel ();
  ~LogSgnErrorRateModel ();

  //Inherited from ErrorRateModel
  bool IsAwgn (void) const;
  int64_t AssignStreams (int64_t stream);

  /// ProtectionMode enumeration
  enum ChannelModelType
  {
    CHANNEL_MODEL_D,
  };

  /**
   * Compute the effective SNR (in linear scale) using the Log-SGN distribution
   * \param snr the RX SNR (in linear scale) after applying path loss
   * \param mcs the MCS used for the transmission
   * \param width the channel width used for the transmission (in MHz)
   * \param nss the number of spatial streams used for the transmission
   * \param nTx the number of antennas used for the transmission
   * \param nRx the number of antennas used for the reception
   * \param muMimoUsers the number of MU-MIMO users that are simultaneously addressed
   * \return the computed effective SNR (in linear scale) using the Log-SGN distribution
   */
  double GetEffectiveSnr (double snr, uint8_t mcs, uint16_t width, uint8_t nss, uint8_t nTx, uint8_t nRx, uint8_t muMimoUsers = 0) const;

private:
  //Inherited from ErrorRateModel
  double DoGetChunkSuccessRate (WifiMode mode, const WifiTxVector& txVector, double snr, uint64_t nbits,
                                uint8_t numRxAntennas, WifiPpduField field, uint16_t staId) const;

  Ptr<NormalRandomVariable> m_normRandomVariable0;    //!< Normal random variable used to compute the normal random variable N(0,1)
  Ptr<NormalRandomVariable> m_normRandomVariable1;    //!< Normal random variable used to compute the Log-SGN random variable
  Ptr<UniformRandomVariable> m_uniformRandomVariable; //!< Uniform random variable used by the mixture model (needed when RX SNR is between 2 available values)

  Ptr<TableBasedErrorRateModel> m_table; //!< Table-based error rate model to check for PER corresponding to effective SNR

  ChannelModelType m_channelModelType; //!< The channel model type
};

} //namespace ns3

#endif /* LOG_SGN_ERROR_RATE_MODEL_H */
