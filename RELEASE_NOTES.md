RELEASE NOTES
=============

This file contains release notes for the wifi-l2s-error-models module (most recent releases first).

Release 1.0
-----------

### New user-visible features

- Initial release of channel model D for SISO configurations

### Compatibility

- ns-3.36.1 release
