/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Sébastien Deronne (sebastien.deronne@gmail.com)
 */

#include "ns3/log.h"
#include "ns3/test.h"
#include "ns3/rng-seed-manager.h"
#include "ns3/wifi-utils.h"
#include "ns3/wifi-phy.h"
#include "ns3/he-phy.h"
#include "ns3/log-sgn-error-rate-model.h"
#include "ns3/log-sgn-matlab-results.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("WifiLogSgnPerTest");

namespace {

// Replacement for std::count until ns-3 supports C++20
// Replace 'CountRuType' with 'std::count' when the latter is supported.
uint32_t
CountRuType (std::vector<HeRu::RuType>::const_iterator itBegin,
             std::vector<HeRu::RuType>::const_iterator itEnd,
             HeRu::RuType ruType)
{
  uint32_t count = 0;
  for (auto it = itBegin; it != itEnd; it++)
    {
      if (*it == ruType)
        {
          count++;
        }
    }
  return count;
}

}

/**
 * \ingroup wifi-log-sgn-test
 * \ingroup tests
 *
 * \brief Log-SGN Error Rate PER for OFDM Test Case
 */
class LogSgnErrorRateOfdmPerTestCase : public TestCase
{
public:
  /**
   * Constructor
   *
   * \param testName the test name
   * \param mode the WifiMode to use for the test
   * \param width the channel width (in MHz) to use for the test
   * \param ldpc whether LDPC is used for the test, BCC is used otherwise
   * \param size the number of bytes to use for the test
   * \param snr the RX SNR (in dB) to consider for the test
   * \param iterations the number of iterations to run for the test
   */
  LogSgnErrorRateOfdmPerTestCase (const std::string &testName, WifiMode mode, uint16_t width, bool ldpc, uint32_t size, double snr, uint64_t iterations);
  virtual ~LogSgnErrorRateOfdmPerTestCase ();

private:
  virtual void DoRun (void);

  std::string m_testName; ///< The name of the test to run
  WifiMode m_mode;        ///< The WifiMode to test
  uint16_t m_width;       ///< The channel width (in MHz) to test
  bool m_ldpc;            ///<  Flag whether LDPC is used for the test, BCC is used otherwise
  double m_snr;           ///< The RX SNR (in dB) to test
  uint32_t m_size;        ///< The size (in bytes) to test
  uint64_t m_iterations;  ///< The number of iterations to run
};

LogSgnErrorRateOfdmPerTestCase::LogSgnErrorRateOfdmPerTestCase (const std::string &testName, WifiMode mode, uint16_t width, bool ldpc, uint32_t size, double snr, uint64_t iterations)
  : TestCase (testName),
    m_testName (testName),
    m_mode (mode),
    m_width (width),
    m_ldpc (ldpc),
    m_snr (snr),
    m_size (size),
    m_iterations (iterations)
{}

LogSgnErrorRateOfdmPerTestCase::~LogSgnErrorRateOfdmPerTestCase ()
{}

void
LogSgnErrorRateOfdmPerTestCase::DoRun (void)
{
  RngSeedManager::SetSeed (1);
  RngSeedManager::SetRun (1);
  int64_t streamNumber = 10;

  LogComponentEnable ("WifiLogSgnPerTest", LOG_LEVEL_ALL);
  //LogComponentEnable ("TableBasedErrorRateModel", LOG_LEVEL_ALL);
  //LogComponentEnable ("LogSgnErrorRateModel", LOG_LEVEL_ALL);

  Ptr<LogSgnErrorRateModel> sgn = CreateObject<LogSgnErrorRateModel> ();
  WifiTxVector txVector;
  txVector.SetMode (m_mode);
  txVector.SetChannelWidth (m_width);
  txVector.SetLdpc (m_ldpc);
  txVector.SetNTx (1);
  txVector.SetNss (1);

  double perFullPhy = 0.0;
  double perLogSgn = 0.0;
  double perMixture = 0.0;
  bool useMixture = false;
  auto fullPhyResults = fullPhySisoResults;
  auto logSgnResults = logSgnSisoResults;
  uint8_t mcs = m_mode.GetMcsValue ();

  auto itFullPhy = fullPhyResults.find (std::make_tuple (mcs, m_width, m_snr));
  if (itFullPhy != fullPhyResults.end ())
    {
      perFullPhy = itFullPhy->second;
    }
  else
    {
      NS_FATAL_ERROR ("SNR value " << m_snr << " not found for " << "SISO" << " MCS-" << +mcs << " and BW " << m_width << " MHz");
    }

  auto itLogSgn = logSgnResults.find (std::make_tuple (mcs, m_width, m_snr));
  if (itLogSgn != logSgnResults.end ())
    {
      perLogSgn = itLogSgn->second;
    }
  else
    {
      NS_FATAL_ERROR ("SNR value " << m_snr << " not found for " << "SISO" << " MCS-" << +mcs << " and BW " << m_width << " MHz");
    }

  auto itMixture = logSgnMixtureSisoResults.find (std::make_tuple (mcs, m_width, m_snr));
  if (itMixture != logSgnMixtureSisoResults.end ())
    {
      perMixture = itMixture->second;
      useMixture = true;
    }

  double psAverage = 0.0;
  double snrAverage = 0.0;
  sgn->AssignStreams (streamNumber);
  for (uint64_t runNumber = 0; runNumber < m_iterations; runNumber++)
    {
      psAverage += sgn->GetChunkSuccessRate (m_mode, txVector, std::pow (10.0, m_snr / 10.0), m_size * 8, 1, WIFI_PPDU_FIELD_DATA);
    }
  sgn->AssignStreams (streamNumber);
  for (uint64_t runNumber = 0; runNumber < m_iterations; runNumber++)
    {
      snrAverage += RatioToDb (sgn->GetEffectiveSnr (m_snr, m_mode.GetMcsValue (), txVector.GetChannelWidth (), txVector.GetNss (), txVector.GetNTx (),  1));
    }
  double ps = psAverage / m_iterations;
  double eff = snrAverage / m_iterations;
  double per = 1 - ps;

  double relativeErrorFullPhy = std::abs (per - perFullPhy) / perFullPhy;
  double relativeErrorLogSgn = std::abs (per - perLogSgn) / perLogSgn;
  double relativeErrorMixture = useMixture ? (std::abs (per - perMixture) / perMixture) : 0.0;
  double deviation = 0.0;
  //if ns-3 PER is not located between full PHY and Log-SGN Matlab PERs,
  //then compute deviation as the minimum between the 2 relative errors calculated previously
  if ((per < std::min (perFullPhy, perLogSgn)) || (per > std::max (perFullPhy, perLogSgn)))
    {
      deviation = std::min (relativeErrorFullPhy, relativeErrorLogSgn);
      //take into account results for mixture model if available
      if (useMixture)
        {
          deviation = std::min (deviation, relativeErrorMixture);
        }
    }

  std::stringstream logInfo;
  logInfo << m_testName << ": snr=" << m_snr << "dB"
          << " avgEffSnr=" << eff << "dB"
          << " per=" << per
          << " perFullPhy=" << perFullPhy
          << " relativeErrorFullPhy=" << relativeErrorFullPhy * 100 << "%"
          << " perLogSgn=" << perLogSgn
          << " relativeErrorLogSgn=" << relativeErrorLogSgn * 100 << "%";
  if (useMixture)
    {
      logInfo << " perMixture=" << perMixture
              << " relativeErrorMixture=" << relativeErrorMixture * 100 << "%";
    }
  logInfo << " deviation=" << deviation * 100 << "%";
  NS_LOG_INFO (logInfo.str ());

  NS_TEST_ASSERT_MSG_LT (deviation, 5e-2, "Deviation is not within tolerance");
}

/**
 * \ingroup wifi-log-sgn-test
 * \ingroup tests
 *
 * \brief Log-SGN Error Rate PER for OFDMA Test Case
 */
class LogSgnErrorRateOfdmaPerTestCase : public TestCase
{
public:
  /**
   * Constructor
   *
   * \param testName the test name
   * \param mcs the HE MCS to use for the test
   * \param ruTypes the RU type per user
   * \param size the number of bytes to use for the test
   * \param snr the RX SNR (in dB) to consider for the test
   * \param iterationsPerUser the number of iterations per user to run for the test
   */
  LogSgnErrorRateOfdmaPerTestCase (const std::string &testName, uint8_t mcs,
                                   const std::vector<HeRu::RuType> &ruTypes,
                                   uint32_t size, double snr,
                                   uint64_t iterationsPerUser);
  virtual ~LogSgnErrorRateOfdmaPerTestCase ();

private:
  virtual void DoRun (void);

  std::string m_testName;              ///< The name of the test to run
  uint8_t m_mcs;                       ///< The MCS to test
  std::vector<HeRu::RuType> m_ruTypes; ///< The RU types for each user (its size defines the number of users)
  double m_snr;                        ///< The RX SNR (in dB) to test
  uint32_t m_size;                     ///< The size (in bytes) to test
  uint64_t m_iterationsPerUser;        ///< The number of iterations to run per user
};

LogSgnErrorRateOfdmaPerTestCase::LogSgnErrorRateOfdmaPerTestCase (const std::string &testName, uint8_t mcs,
                                                                  const std::vector<HeRu::RuType> &ruTypes,
                                                                  uint32_t size, double snr,
                                                                  uint64_t iterationsPerUser)
  : TestCase (testName),
    m_testName (testName),
    m_mcs (mcs),
    m_ruTypes (ruTypes),
    m_snr (snr),
    m_size (size),
    m_iterationsPerUser (iterationsPerUser)
{}

LogSgnErrorRateOfdmaPerTestCase::~LogSgnErrorRateOfdmaPerTestCase ()
{}

void
LogSgnErrorRateOfdmaPerTestCase::DoRun (void)
{
  RngSeedManager::SetSeed (1);
  RngSeedManager::SetRun (1);
  int64_t streamNumber = 100;

  LogComponentEnable ("WifiLogSgnPerTest", LOG_LEVEL_ALL);
  //LogComponentEnable ("TableBasedErrorRateModel", LOG_LEVEL_ALL);
  //LogComponentEnable ("LogSgnErrorRateModel", LOG_LEVEL_ALL);

  std::vector<Ptr<LogSgnErrorRateModel> > logSgnErrorRateModels;
  logSgnErrorRateModels.reserve (m_ruTypes.size ());

  std::vector<WifiTxVector> txVectors;
  txVectors.reserve (m_ruTypes.size ());

  for (std::size_t i = 0; i < m_ruTypes.size (); i++)
    {
      Ptr<LogSgnErrorRateModel> sgn = CreateObject<LogSgnErrorRateModel> ();
      sgn->AssignStreams (streamNumber++);
      logSgnErrorRateModels.push_back (sgn);

      WifiTxVector txVector = WifiTxVector (HePhy::GetHeMcs (m_mcs), 0, WIFI_PREAMBLE_HE_MU, 800, 1,  1, 0, 20, false, false, true);
      HeRu::RuSpec ru (m_ruTypes[i], i + 1, true);
      txVector.SetRu (ru, i);
      txVector.SetMode (HePhy::GetHeMcs (m_mcs), i);
      txVector.SetNss (1, i);
      txVectors.push_back (txVector);
    }

  auto fullPhyResults = fullPhySisoResults;
  auto logSgnResults = logSgnSisoResults;
  for (std::size_t i = 0; i < m_ruTypes.size (); i++)
    {
      double perFullPhy = 0.0;
      double perLogSgn = 0.0;

      uint16_t width = HeRu::GetBandwidth (txVectors[i].GetRu (i).GetRuType ());

      auto itFullPhy = fullPhyResults.find (std::make_tuple (m_mcs, width, m_snr));
      if (itFullPhy != fullPhyResults.end ())
        {
          perFullPhy = itFullPhy->second;
        }
      else
        {
          NS_FATAL_ERROR ("SNR value " << m_snr << " not found for " << "SISO" << " MCS-" << +m_mcs << " and BW " << width << " MHz");
        }

      auto itLogSgn = logSgnResults.find (std::make_tuple (m_mcs, width, m_snr));
      if (itLogSgn != logSgnResults.end ())
        {
          perLogSgn = itLogSgn->second;
        }
      else
        {
          NS_FATAL_ERROR ("SNR value " << m_snr << " not found for " << "SISO" << " MCS-" << +m_mcs << " and BW " << width << " MHz");
        }

      double psAverage = 0.0;
      Ptr<LogSgnErrorRateModel> sgn = logSgnErrorRateModels[i];
      for (uint64_t runNumber = 0; runNumber < m_iterationsPerUser; runNumber++)
        {
          psAverage += sgn->GetChunkSuccessRate (HePhy::GetHeMcs (m_mcs), txVectors[i], std::pow (10.0, m_snr / 10.0), m_size * 8, 1, WIFI_PPDU_FIELD_DATA, i);
        }
      double ps = psAverage / m_iterationsPerUser;
      double per = 1 - ps;

      double relativeErrorFullPhy = std::abs (per - perFullPhy) / perFullPhy;
      double relativeErrorLogSgn = std::abs (per - perLogSgn) / perLogSgn;
      double deviation = 0.0;
      //if ns-3 PER is not located between full PHY and Log-SGN Matlab PERs,
      //then compute deviation as the minimum between the 2 relative errors calculated previously
      if ((per < std::min (perFullPhy, perLogSgn)) || (per > std::max (perFullPhy, perLogSgn)))
        {
          deviation = std::min (relativeErrorFullPhy, relativeErrorLogSgn);
        }

      std::stringstream logInfo;
      NS_LOG_INFO (m_testName << ": user #" << i + 1
                              << " ru=" << txVectors[i].GetRu (i).GetRuType ()
                              << " snr=" << m_snr << "dB"
                              << " per=" << per
                              << " perFullPhy=" << perFullPhy
                              << " relativeErrorFullPhy=" << relativeErrorFullPhy * 100 << "%"
                              << " perLogSgn=" << perLogSgn
                              << " relativeErrorLogSgn=" << relativeErrorLogSgn * 100 << "%"
                              << " deviation=" << deviation * 100 << "%");

      NS_TEST_ASSERT_MSG_LT (deviation, 5e-2, "Deviation is not within tolerance");
    }
}

/**
 * \ingroup wifi-log-sgn-test
 * \ingroup tests
 *
 * \brief Wi-Fi Log-SGN PER Test Suite
 */
class WifiLogSgnPerTestSuite : public TestSuite
{
public:
  WifiLogSgnPerTestSuite ();
};

WifiLogSgnPerTestSuite::WifiLogSgnPerTestSuite ()
  : TestSuite ("wifi-log-sgn-per", UNIT)
{
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes--3db", HePhy::GetHeMcs0 (), 20, true, 1000, -3, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes--2db", HePhy::GetHeMcs0 (), 20, true, 1000, -2, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes--1db", HePhy::GetHeMcs0 (), 20, true, 1000, -1, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-0db", HePhy::GetHeMcs0 (), 20, true, 1000, 0, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-1db", HePhy::GetHeMcs0 (), 20, true, 1000, 1, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-2db", HePhy::GetHeMcs0 (), 20, true, 1000, 2, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-3db", HePhy::GetHeMcs0 (), 20, true, 1000, 3, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-4db", HePhy::GetHeMcs0 (), 20, true, 1000, 4, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-5db", HePhy::GetHeMcs0 (), 20, true, 1000, 5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-6db", HePhy::GetHeMcs0 (), 20, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-7db", HePhy::GetHeMcs0 (), 20, true, 1000, 7, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS0-LDPC-1000bytes-8db", HePhy::GetHeMcs0 (), 20, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS1-LDPC-1000bytes-0db", HePhy::GetHeMcs1 (), 20, true, 1000, 0, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS1-LDPC-1000bytes-4db", HePhy::GetHeMcs1 (), 20, true, 1000, 4, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS1-LDPC-1000bytes-8db", HePhy::GetHeMcs1 (), 20, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS2-LDPC-1000bytes-2db", HePhy::GetHeMcs2 (), 20, true, 1000, 2, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS2-LDPC-1000bytes-6db", HePhy::GetHeMcs2 (), 20, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS2-LDPC-1000bytes-10db", HePhy::GetHeMcs2 (), 20, true, 1000, 10, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS2-LDPC-1000bytes-14db", HePhy::GetHeMcs2 (), 20, true, 1000, 14, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS3-LDPC-1000bytes-8db", HePhy::GetHeMcs3 (), 20, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS3-LDPC-1000bytes-12db", HePhy::GetHeMcs3 (), 20, true, 1000, 12, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS3-LDPC-1000bytes-16db", HePhy::GetHeMcs3 (), 20, true, 1000, 16, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS4-LDPC-1000bytes-11db", HePhy::GetHeMcs4 (), 20, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS4-LDPC-1000bytes-15db", HePhy::GetHeMcs4 (), 20, true, 1000, 15, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS4-LDPC-1000bytes-19db", HePhy::GetHeMcs4 (), 20, true, 1000, 19, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS5-LDPC-1000bytes-14db", HePhy::GetHeMcs5 (), 20, true, 1000, 14, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS5-LDPC-1000bytes-18db", HePhy::GetHeMcs5 (), 20, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS5-LDPC-1000bytes-22db", HePhy::GetHeMcs5 (), 20, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS6-LDPC-1000bytes-16db", HePhy::GetHeMcs6 (), 20, true, 1000, 16, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS6-LDPC-1000bytes-20db", HePhy::GetHeMcs6 (), 20, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS6-LDPC-1000bytes-24db", HePhy::GetHeMcs6 (), 20, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS7-LDPC-1000bytes-18db", HePhy::GetHeMcs7 (), 20, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS7-LDPC-1000bytes-22db", HePhy::GetHeMcs7 (), 20, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS7-LDPC-1000bytes-26db", HePhy::GetHeMcs7 (), 20, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS8-LDPC-1000bytes-21.5db", HePhy::GetHeMcs8 (), 20, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS8-LDPC-1000bytes-25.5db", HePhy::GetHeMcs8 (), 20, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS8-LDPC-1000bytes-29.5db", HePhy::GetHeMcs8 (), 20, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS9-LDPC-1000bytes-20db", HePhy::GetHeMcs9 (), 20, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS9-LDPC-1000bytes-24db", HePhy::GetHeMcs9 (), 20, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS9-LDPC-1000bytes-28db", HePhy::GetHeMcs9 (), 20, true, 1000, 28, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS9-LDPC-1000bytes-32db", HePhy::GetHeMcs9 (), 20, true, 1000, 32, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-21.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-23.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 23.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-25.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-27.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 27.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-29.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-31.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 31.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-32db", HePhy::GetHeMcs10 (), 20, true, 1000, 32, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-32.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 32.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-33db", HePhy::GetHeMcs10 (), 20, true, 1000, 33, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-33.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 33.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-34db", HePhy::GetHeMcs10 (), 20, true, 1000, 34, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS10-LDPC-1000bytes-34.5db", HePhy::GetHeMcs10 (), 20, true, 1000, 34.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-22db", HePhy::GetHeMcs11 (), 20, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-26db", HePhy::GetHeMcs11 (), 20, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-30db", HePhy::GetHeMcs11 (), 20, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-34db", HePhy::GetHeMcs11 (), 20, true, 1000, 34, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-34.5db", HePhy::GetHeMcs11 (), 20, true, 1000, 34.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-35db", HePhy::GetHeMcs11 (), 20, true, 1000, 35, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-35.5db", HePhy::GetHeMcs11 (), 20, true, 1000, 35.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-36db", HePhy::GetHeMcs11 (), 20, true, 1000, 36, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-36.5db", HePhy::GetHeMcs11 (), 20, true, 1000, 36.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-37db", HePhy::GetHeMcs11 (), 20, true, 1000, 37, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-37.5db", HePhy::GetHeMcs11 (), 20, true, 1000, 37.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-20MHz-SU-SISO-MCS11-LDPC-1000bytes-38db", HePhy::GetHeMcs11 (), 20, true, 1000, 38, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS0-LDPC-1000bytes--1.5db", HePhy::GetHeMcs0 (), 40, true, 1000, -1.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS0-LDPC-1000bytes-1.5db", HePhy::GetHeMcs0 (), 40, true, 1000, 1.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS0-LDPC-1000bytes-3db", HePhy::GetHeMcs0 (), 40, true, 1000, 3, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS0-LDPC-1000bytes-4.5db", HePhy::GetHeMcs0 (), 40, true, 1000, 4.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS0-LDPC-1000bytes-6db", HePhy::GetHeMcs0 (), 40, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS1-LDPC-1000bytes-0db", HePhy::GetHeMcs1 (), 40, true, 1000, 0, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS1-LDPC-1000bytes-4db", HePhy::GetHeMcs1 (), 40, true, 1000, 4, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS1-LDPC-1000bytes-6db", HePhy::GetHeMcs1 (), 40, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS1-LDPC-1000bytes-8db", HePhy::GetHeMcs1 (), 40, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS1-LDPC-1000bytes-10db", HePhy::GetHeMcs1 (), 40, true, 1000, 10, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS2-LDPC-1000bytes-2db", HePhy::GetHeMcs2 (), 40, true, 1000, 2, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS2-LDPC-1000bytes-6db", HePhy::GetHeMcs2 (), 40, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS2-LDPC-1000bytes-10db", HePhy::GetHeMcs2 (), 40, true, 1000, 10, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS2-LDPC-1000bytes-12.5db", HePhy::GetHeMcs2 (), 40, true, 1000, 12.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS2-LDPC-1000bytes-13db", HePhy::GetHeMcs2 (), 40, true, 1000, 13, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS2-LDPC-1000bytes-13.5db", HePhy::GetHeMcs2 (), 40, true, 1000, 13.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS3-LDPC-1000bytes-8db", HePhy::GetHeMcs3 (), 40, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS3-LDPC-1000bytes-12db", HePhy::GetHeMcs3 (), 40, true, 1000, 12, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS3-LDPC-1000bytes-14db", HePhy::GetHeMcs3 (), 40, true, 1000, 14, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS3-LDPC-1000bytes-14.5db", HePhy::GetHeMcs3 (), 40, true, 1000, 14.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS3-LDPC-1000bytes-15db", HePhy::GetHeMcs3 (), 40, true, 1000, 15, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS3-LDPC-1000bytes-15.5db", HePhy::GetHeMcs3 (), 40, true, 1000, 15.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS4-LDPC-1000bytes-11db", HePhy::GetHeMcs4 (), 40, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS4-LDPC-1000bytes-15db", HePhy::GetHeMcs4 (), 40, true, 1000, 15, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS4-LDPC-1000bytes-17db", HePhy::GetHeMcs4 (), 40, true, 1000, 17, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS4-LDPC-1000bytes-19db", HePhy::GetHeMcs4 (), 40, true, 1000, 19, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS5-LDPC-1000bytes-14db", HePhy::GetHeMcs5 (), 40, true, 1000, 14, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS5-LDPC-1000bytes-18db", HePhy::GetHeMcs5 (), 40, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS5-LDPC-1000bytes-22db", HePhy::GetHeMcs5 (), 40, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS5-LDPC-1000bytes-22.5db", HePhy::GetHeMcs5 (), 40, true, 1000, 22.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS5-LDPC-1000bytes-23db", HePhy::GetHeMcs5 (), 40, true, 1000, 23, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS6-LDPC-1000bytes-16db", HePhy::GetHeMcs6 (), 40, true, 1000, 16, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS6-LDPC-1000bytes-20db", HePhy::GetHeMcs6 (), 40, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS6-LDPC-1000bytes-22db", HePhy::GetHeMcs6 (), 40, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS6-LDPC-1000bytes-24db", HePhy::GetHeMcs6 (), 40, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS6-LDPC-1000bytes-26db", HePhy::GetHeMcs6 (), 40, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS7-LDPC-1000bytes-18db", HePhy::GetHeMcs7 (), 40, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS7-LDPC-1000bytes-22db", HePhy::GetHeMcs7 (), 40, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS7-LDPC-1000bytes-24db", HePhy::GetHeMcs7 (), 40, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS7-LDPC-1000bytes-26db", HePhy::GetHeMcs7 (), 40, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-21.5db", HePhy::GetHeMcs8 (), 40, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-25.5db", HePhy::GetHeMcs8 (), 40, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-28db", HePhy::GetHeMcs8 (), 40, true, 1000, 28, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-28.5db", HePhy::GetHeMcs8 (), 40, true, 1000, 28.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-29db", HePhy::GetHeMcs8 (), 40, true, 1000, 29, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-29.5db", HePhy::GetHeMcs8 (), 40, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-30db", HePhy::GetHeMcs8 (), 40, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS8-LDPC-1000bytes-30.5db", HePhy::GetHeMcs8 (), 40, true, 1000, 30.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-20db", HePhy::GetHeMcs9 (), 40, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-24db", HePhy::GetHeMcs9 (), 40, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-28db", HePhy::GetHeMcs9 (), 40, true, 1000, 28, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-30db", HePhy::GetHeMcs9 (), 40, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-30.5db", HePhy::GetHeMcs9 (), 40, true, 1000, 30.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-31db", HePhy::GetHeMcs9 (), 40, true, 1000, 31, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-31.5db", HePhy::GetHeMcs9 (), 40, true, 1000, 31.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-32db", HePhy::GetHeMcs9 (), 40, true, 1000, 32, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS9-LDPC-1000bytes-32.5db", HePhy::GetHeMcs9 (), 40, true, 1000, 32.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-21.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-23.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 23.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-25.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-27.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 27.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-29.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-31.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 31.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-32db", HePhy::GetHeMcs10 (), 40, true, 1000, 32, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-32.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 32.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-33db", HePhy::GetHeMcs10 (), 40, true, 1000, 33, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-33.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 33.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-34db", HePhy::GetHeMcs10 (), 40, true, 1000, 34, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS10-LDPC-1000bytes-34.5db", HePhy::GetHeMcs10 (), 40, true, 1000, 34.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-22db", HePhy::GetHeMcs11 (), 40, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-26db", HePhy::GetHeMcs11 (), 40, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-30db", HePhy::GetHeMcs11 (), 40, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-34db", HePhy::GetHeMcs11 (), 40, true, 1000, 34, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-34.5db", HePhy::GetHeMcs11 (), 40, true, 1000, 34.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-35db", HePhy::GetHeMcs11 (), 40, true, 1000, 35, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-35.5db", HePhy::GetHeMcs11 (), 40, true, 1000, 35.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-36db", HePhy::GetHeMcs11 (), 40, true, 1000, 36, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-36.5db", HePhy::GetHeMcs11 (), 40, true, 1000, 36.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-37db", HePhy::GetHeMcs11 (), 40, true, 1000, 37, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-37.5db", HePhy::GetHeMcs11 (), 40, true, 1000, 37.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-40MHz-SU-SISO-MCS11-LDPC-1000bytes-38db", HePhy::GetHeMcs11 (), 40, true, 1000, 38, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes--1.5db", HePhy::GetHeMcs0 (), 80, true, 1000, -1.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-1.5db", HePhy::GetHeMcs0 (), 80, true, 1000, 1.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-2db", HePhy::GetHeMcs0 (), 80, true, 1000, 2, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-2.5db", HePhy::GetHeMcs0 (), 80, true, 1000, 2.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-3db", HePhy::GetHeMcs0 (), 80, true, 1000, 3, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-3.5db", HePhy::GetHeMcs0 (), 80, true, 1000, 3.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-4db", HePhy::GetHeMcs0 (), 80, true, 1000, 4, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-4.5db", HePhy::GetHeMcs0 (), 80, true, 1000, 4.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS0-LDPC-1000bytes-5db", HePhy::GetHeMcs0 (), 80, true, 1000, 5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS1-LDPC-1000bytes-0db", HePhy::GetHeMcs1 (), 80, true, 1000, 0, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS1-LDPC-1000bytes-4db", HePhy::GetHeMcs1 (), 80, true, 1000, 4, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS1-LDPC-1000bytes-6db", HePhy::GetHeMcs1 (), 80, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS1-LDPC-1000bytes-6.5db", HePhy::GetHeMcs1 (), 80, true, 1000, 6.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS1-LDPC-1000bytes-7db", HePhy::GetHeMcs1 (), 80, true, 1000, 7, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS1-LDPC-1000bytes-7.5db", HePhy::GetHeMcs1 (), 80, true, 1000, 7.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS1-LDPC-1000bytes-8db", HePhy::GetHeMcs1 (), 80, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-2db", HePhy::GetHeMcs2 (), 80, true, 1000, 2, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-6db", HePhy::GetHeMcs2 (), 80, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-10db", HePhy::GetHeMcs2 (), 80, true, 1000, 10, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-11db", HePhy::GetHeMcs2 (), 80, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-11.5db", HePhy::GetHeMcs2 (), 80, true, 1000, 11.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-12db", HePhy::GetHeMcs2 (), 80, true, 1000, 12, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-12.5db", HePhy::GetHeMcs2 (), 80, true, 1000, 12.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-13db", HePhy::GetHeMcs2 (), 80, true, 1000, 13, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS2-LDPC-1000bytes-13.5db", HePhy::GetHeMcs2 (), 80, true, 1000, 13.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-8db", HePhy::GetHeMcs3 (), 80, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-10db", HePhy::GetHeMcs3 (), 80, true, 1000, 10, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-11db", HePhy::GetHeMcs3 (), 80, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-12db", HePhy::GetHeMcs3 (), 80, true, 1000, 12, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-12.5db", HePhy::GetHeMcs3 (), 80, true, 1000, 12.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-13db", HePhy::GetHeMcs3 (), 80, true, 1000, 13, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-13.5db", HePhy::GetHeMcs3 (), 80, true, 1000, 13.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS3-LDPC-1000bytes-14db", HePhy::GetHeMcs3 (), 80, true, 1000, 14, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-11db", HePhy::GetHeMcs4 (), 80, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-15db", HePhy::GetHeMcs4 (), 80, true, 1000, 15, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-16db", HePhy::GetHeMcs4 (), 80, true, 1000, 16, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-17db", HePhy::GetHeMcs4 (), 80, true, 1000, 17, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-17.5db", HePhy::GetHeMcs4 (), 80, true, 1000, 17.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-18db", HePhy::GetHeMcs4 (), 80, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-18.5db", HePhy::GetHeMcs4 (), 80, true, 1000, 18.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS4-LDPC-1000bytes-19db", HePhy::GetHeMcs4 (), 80, true, 1000, 19, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-14db", HePhy::GetHeMcs5 (), 80, true, 1000, 14, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-18db", HePhy::GetHeMcs5 (), 80, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-19db", HePhy::GetHeMcs5 (), 80, true, 1000, 19, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-19.5db", HePhy::GetHeMcs5 (), 80, true, 1000, 19.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-20db", HePhy::GetHeMcs5 (), 80, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-20.5db", HePhy::GetHeMcs5 (), 80, true, 1000, 20.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-21db", HePhy::GetHeMcs5 (), 80, true, 1000, 21, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-21.5db", HePhy::GetHeMcs5 (), 80, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS5-LDPC-1000bytes-22db", HePhy::GetHeMcs5 (), 80, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-16db", HePhy::GetHeMcs6 (), 80, true, 1000, 16, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-20db", HePhy::GetHeMcs6 (), 80, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-20.5db", HePhy::GetHeMcs6 (), 80, true, 1000, 20.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-21db", HePhy::GetHeMcs6 (), 80, true, 1000, 21, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-21.5db", HePhy::GetHeMcs6 (), 80, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-22db", HePhy::GetHeMcs6 (), 80, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-22.5db", HePhy::GetHeMcs6 (), 80, true, 1000, 22.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS6-LDPC-1000bytes-23db", HePhy::GetHeMcs6 (), 80, true, 1000, 23, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-18db", HePhy::GetHeMcs7 (), 80, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-22db", HePhy::GetHeMcs7 (), 80, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-23db", HePhy::GetHeMcs7 (), 80, true, 1000, 23, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-23.5db", HePhy::GetHeMcs7 (), 80, true, 1000, 23.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-24db", HePhy::GetHeMcs7 (), 80, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-24.5db", HePhy::GetHeMcs7 (), 80, true, 1000, 24.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-25db", HePhy::GetHeMcs7 (), 80, true, 1000, 25, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-25.5db", HePhy::GetHeMcs7 (), 80, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS7-LDPC-1000bytes-26db", HePhy::GetHeMcs7 (), 80, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-21.5db", HePhy::GetHeMcs8 (), 80, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-23db", HePhy::GetHeMcs8 (), 80, true, 1000, 23, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-24.5db", HePhy::GetHeMcs8 (), 80, true, 1000, 24.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-25.5db", HePhy::GetHeMcs8 (), 80, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-26db", HePhy::GetHeMcs8 (), 80, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-26.5db", HePhy::GetHeMcs8 (), 80, true, 1000, 26.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-27db", HePhy::GetHeMcs8 (), 80, true, 1000, 27, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS8-LDPC-1000bytes-27.5db", HePhy::GetHeMcs8 (), 80, true, 1000, 27.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-20db", HePhy::GetHeMcs9 (), 80, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-24db", HePhy::GetHeMcs9 (), 80, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-26db", HePhy::GetHeMcs9 (), 80, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-28db", HePhy::GetHeMcs9 (), 80, true, 1000, 28, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-28.5db", HePhy::GetHeMcs9 (), 80, true, 1000, 28.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-29db", HePhy::GetHeMcs9 (), 80, true, 1000, 29, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-29.5db", HePhy::GetHeMcs9 (), 80, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-30db", HePhy::GetHeMcs9 (), 80, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS9-LDPC-1000bytes-30.5db", HePhy::GetHeMcs9 (), 80, true, 1000, 30.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-21.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-23.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 23.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-25.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-27.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 27.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-29.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-31.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 31.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-32db", HePhy::GetHeMcs10 (), 80, true, 1000, 32, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-32.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 32.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-33db", HePhy::GetHeMcs10 (), 80, true, 1000, 33, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-33.5db", HePhy::GetHeMcs10 (), 80, true, 1000, 33.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS10-LDPC-1000bytes-34db", HePhy::GetHeMcs10 (), 80, true, 1000, 34, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS11-LDPC-1000bytes-22db", HePhy::GetHeMcs11 (), 80, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS11-LDPC-1000bytes-26db", HePhy::GetHeMcs11 (), 80, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS11-LDPC-1000bytes-30db", HePhy::GetHeMcs11 (), 80, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS11-LDPC-1000bytes-34db", HePhy::GetHeMcs11 (), 80, true, 1000, 34, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS11-LDPC-1000bytes-34.5db", HePhy::GetHeMcs11 (), 80, true, 1000, 34.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS11-LDPC-1000bytes-35db", HePhy::GetHeMcs11 (), 80, true, 1000, 35, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-80MHz-SU-SISO-MCS11-LDPC-1000bytes-35.5db", HePhy::GetHeMcs11 (), 80, true, 1000, 35.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS0-LDPC-1000bytes--1.5db", HePhy::GetHeMcs0 (), 160, true, 1000, -1.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS0-LDPC-1000bytes-1.5db", HePhy::GetHeMcs0 (), 160, true, 1000, 1.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS0-LDPC-1000bytes-2db", HePhy::GetHeMcs0 (), 160, true, 1000, 2, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS0-LDPC-1000bytes-2.5db", HePhy::GetHeMcs0 (), 160, true, 1000, 2.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS0-LDPC-1000bytes-3.5db", HePhy::GetHeMcs0 (), 160, true, 1000, 3.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS0-LDPC-1000bytes-4db", HePhy::GetHeMcs0 (), 160, true, 1000, 4, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS1-LDPC-1000bytes-0db", HePhy::GetHeMcs1 (), 160, true, 1000, 0, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS1-LDPC-1000bytes-4db", HePhy::GetHeMcs1 (), 160, true, 1000, 4, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS1-LDPC-1000bytes-6db", HePhy::GetHeMcs1 (), 160, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS1-LDPC-1000bytes-6.5db", HePhy::GetHeMcs1 (), 160, true, 1000, 6.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS1-LDPC-1000bytes-7db", HePhy::GetHeMcs1 (), 160, true, 1000, 7, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS1-LDPC-1000bytes-7.5db", HePhy::GetHeMcs1 (), 160, true, 1000, 7.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS1-LDPC-1000bytes-8.5db", HePhy::GetHeMcs1 (), 160, true, 1000, 8.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS2-LDPC-1000bytes-6db", HePhy::GetHeMcs2 (), 160, true, 1000, 6, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS2-LDPC-1000bytes-10db", HePhy::GetHeMcs2 (), 160, true, 1000, 10, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS2-LDPC-1000bytes-11db", HePhy::GetHeMcs2 (), 160, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS2-LDPC-1000bytes-11.5db", HePhy::GetHeMcs2 (), 160, true, 1000, 11.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS2-LDPC-1000bytes-12db", HePhy::GetHeMcs2 (), 160, true, 1000, 12, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS2-LDPC-1000bytes-12.5db", HePhy::GetHeMcs2 (), 160, true, 1000, 12.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS2-LDPC-1000bytes-13db", HePhy::GetHeMcs2 (), 160, true, 1000, 13, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS3-LDPC-1000bytes-8db", HePhy::GetHeMcs3 (), 160, true, 1000, 8, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS3-LDPC-1000bytes-10db", HePhy::GetHeMcs3 (), 160, true, 1000, 10, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS3-LDPC-1000bytes-11db", HePhy::GetHeMcs3 (), 160, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS3-LDPC-1000bytes-12db", HePhy::GetHeMcs3 (), 160, true, 1000, 12, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS3-LDPC-1000bytes-13db", HePhy::GetHeMcs3 (), 160, true, 1000, 13, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS4-LDPC-1000bytes-11db", HePhy::GetHeMcs4 (), 160, true, 1000, 11, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS4-LDPC-1000bytes-15db", HePhy::GetHeMcs4 (), 160, true, 1000, 15, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS4-LDPC-1000bytes-17db", HePhy::GetHeMcs4 (), 160, true, 1000, 17, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS4-LDPC-1000bytes-18db", HePhy::GetHeMcs4 (), 160, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS4-LDPC-1000bytes-18.5db", HePhy::GetHeMcs4 (), 160, true, 1000, 18.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-14db", HePhy::GetHeMcs5 (), 160, true, 1000, 14, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-18db", HePhy::GetHeMcs5 (), 160, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-20db", HePhy::GetHeMcs5 (), 160, true, 1000, 20, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-20.5db", HePhy::GetHeMcs5 (), 160, true, 1000, 20.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-21db", HePhy::GetHeMcs5 (), 160, true, 1000, 21, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-21.5db", HePhy::GetHeMcs5 (), 160, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-22db", HePhy::GetHeMcs5 (), 160, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS5-LDPC-1000bytes-22.5db", HePhy::GetHeMcs5 (), 160, true, 1000, 22.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS6-LDPC-1000bytes-16db", HePhy::GetHeMcs6 (), 160, true, 1000, 16, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS6-LDPC-1000bytes-19db", HePhy::GetHeMcs6 (), 160, true, 1000, 19, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS6-LDPC-1000bytes-20.5db", HePhy::GetHeMcs6 (), 160, true, 1000, 20.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS6-LDPC-1000bytes-21db", HePhy::GetHeMcs6 (), 160, true, 1000, 21, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS6-LDPC-1000bytes-21.5db", HePhy::GetHeMcs6 (), 160, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS6-LDPC-1000bytes-22.5db", HePhy::GetHeMcs6 (), 160, true, 1000, 22.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-18db", HePhy::GetHeMcs7 (), 160, true, 1000, 18, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-22db", HePhy::GetHeMcs7 (), 160, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-23.5db", HePhy::GetHeMcs7 (), 160, true, 1000, 23.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-24.5db", HePhy::GetHeMcs7 (), 160, true, 1000, 24.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-25db", HePhy::GetHeMcs7 (), 160, true, 1000, 25, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-25.5db", HePhy::GetHeMcs7 (), 160, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-26db", HePhy::GetHeMcs7 (), 160, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS7-LDPC-1000bytes-26.5db", HePhy::GetHeMcs7 (), 160, true, 1000, 26.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS8-LDPC-1000bytes-21db", HePhy::GetHeMcs8 (), 160, true, 1000, 21, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS8-LDPC-1000bytes-24db", HePhy::GetHeMcs8 (), 160, true, 1000, 24, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS8-LDPC-1000bytes-26db", HePhy::GetHeMcs8 (), 160, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS8-LDPC-1000bytes-26.5db", HePhy::GetHeMcs8 (), 160, true, 1000, 26.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS8-LDPC-1000bytes-27db", HePhy::GetHeMcs8 (), 160, true, 1000, 27, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-22db", HePhy::GetHeMcs9 (), 160, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-26db", HePhy::GetHeMcs9 (), 160, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-28.5db", HePhy::GetHeMcs9 (), 160, true, 1000, 28.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-29db", HePhy::GetHeMcs9 (), 160, true, 1000, 29, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-29.5db", HePhy::GetHeMcs9 (), 160, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-30db", HePhy::GetHeMcs9 (), 160, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-30.5db", HePhy::GetHeMcs9 (), 160, true, 1000, 30.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS9-LDPC-1000bytes-31db", HePhy::GetHeMcs9 (), 160, true, 1000, 31, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS10-LDPC-1000bytes-21.5db", HePhy::GetHeMcs10 (), 160, true, 1000, 21.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS10-LDPC-1000bytes-23.5db", HePhy::GetHeMcs10 (), 160, true, 1000, 23.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS10-LDPC-1000bytes-25.5db", HePhy::GetHeMcs10 (), 160, true, 1000, 25.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS10-LDPC-1000bytes-27.5db", HePhy::GetHeMcs10 (), 160, true, 1000, 27.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS10-LDPC-1000bytes-29.5db", HePhy::GetHeMcs10 (), 160, true, 1000, 29.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS10-LDPC-1000bytes-31.5db", HePhy::GetHeMcs10 (), 160, true, 1000, 31.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS10-LDPC-1000bytes-32db", HePhy::GetHeMcs10 (), 160, true, 1000, 32, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-22db", HePhy::GetHeMcs11 (), 160, true, 1000, 22, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-26db", HePhy::GetHeMcs11 (), 160, true, 1000, 26, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-30db", HePhy::GetHeMcs11 (), 160, true, 1000, 30, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-34db", HePhy::GetHeMcs11 (), 160, true, 1000, 34, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-34.5db", HePhy::GetHeMcs11 (), 160, true, 1000, 34.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-35db", HePhy::GetHeMcs11 (), 160, true, 1000, 35, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-35.5db", HePhy::GetHeMcs11 (), 160, true, 1000, 35.5, 40000), TestCase::QUICK);
  AddTestCase (new LogSgnErrorRateOfdmPerTestCase ("LogSgn-11ax-160MHz-SU-SISO-MCS11-LDPC-1000bytes-36db", HePhy::GetHeMcs11 (), 160, true, 1000, 36, 40000), TestCase::QUICK);

  std::vector<std::vector<HeRu::RuType> > scenarios;

  std::vector<HeRu::RuType> scenario0 (9);
  std::fill (scenario0.begin (), scenario0.end (), HeRu::RuType::RU_26_TONE);
  scenarios.push_back (scenario0);

  std::vector<HeRu::RuType> scenario112 (4);
  std::fill (scenario112.begin (), scenario112.end (), HeRu::RuType::RU_52_TONE);
  scenarios.push_back (scenario112);

  std::vector<HeRu::RuType> scenario96 (2);
  std::fill (scenario96.begin (), scenario96.end (), HeRu::RuType::RU_106_TONE);
  scenarios.push_back (scenario96);

  for (const auto & scenario : scenarios)
    {
      if (CountRuType (scenario.begin (), scenario.end (), HeRu::RuType::RU_26_TONE) == 0)
        {
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-1000bytes--3db", 0, scenario, 1000, -3, 40000), TestCase::QUICK);
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-1000bytes-0db", 0, scenario, 1000, 0, 40000), TestCase::QUICK);
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-1000bytes-3db", 0, scenario, 1000, 3, 40000), TestCase::QUICK);
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-1000bytes-6db", 0, scenario, 1000, 6, 40000), TestCase::QUICK);
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-1000bytes-9db", 0, scenario, 1000, 9, 40000), TestCase::QUICK);
        }
      else
        {
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-503bytes--3db", 0, scenario, 503, -3, 40000), TestCase::QUICK);
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-503bytes-0db", 0, scenario, 503, 0, 40000), TestCase::QUICK);
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS0-LDPC-503bytes-3db", 0, scenario, 503, 3, 40000), TestCase::QUICK);
        }

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS1-LDPC-1000bytes-0db", 1, scenario, 1000, 0, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS1-LDPC-1000bytes-4db", 1, scenario, 1000, 4, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS1-LDPC-1000bytes-8db", 1, scenario, 1000, 8, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS1-LDPC-1000bytes-12db", 1, scenario, 1000, 12, 40000), TestCase::QUICK);

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS2-LDPC-1000bytes-2db", 2, scenario, 1000, 2, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS2-LDPC-1000bytes-6db", 2, scenario, 1000, 6, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS2-LDPC-1000bytes-10db", 2, scenario, 1000, 10, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS2-LDPC-1000bytes-14db", 2, scenario, 1000, 14, 40000), TestCase::QUICK);
      if (CountRuType (scenario.begin (), scenario.end (), HeRu::RuType::RU_106_TONE) == 0)
        {
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS2-LDPC-1000bytes-18db", 2, scenario, 1000, 18, 40000), TestCase::QUICK);
        }

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS3-LDPC-1000bytes-8db", 3, scenario, 1000, 8, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS3-LDPC-1000bytes-12db", 3, scenario, 1000, 12, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS3-LDPC-1000bytes-16db", 3, scenario, 1000, 16, 40000), TestCase::QUICK);
      if (CountRuType (scenario.begin (), scenario.end (), HeRu::RuType::RU_106_TONE) == 0)
        {
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS3-LDPC-1000bytes-20db", 3, scenario, 1000, 20, 40000), TestCase::QUICK);
        }

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS4-LDPC-1000bytes-11db", 4, scenario, 1000, 11, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS4-LDPC-1000bytes-15db", 4, scenario, 1000, 15, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS4-LDPC-1000bytes-19db", 4, scenario, 1000, 19, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS4-LDPC-1000bytes-22db", 4, scenario, 1000, 22, 40000), TestCase::QUICK);

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS5-LDPC-1000bytes-14db", 5, scenario, 1000, 14, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS5-LDPC-1000bytes-18db", 5, scenario, 1000, 18, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS5-LDPC-1000bytes-22db", 5, scenario, 1000, 22, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS5-LDPC-1000bytes-26db", 5, scenario, 1000, 26, 40000), TestCase::QUICK);
      if (CountRuType (scenario.begin (), scenario.end (), HeRu::RuType::RU_106_TONE) == 0)
        {
          AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS5-LDPC-1000bytes-28db", 5, scenario, 1000, 28, 40000), TestCase::QUICK);
        }

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS6-LDPC-1000bytes-16db", 6, scenario, 1000, 16, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS6-LDPC-1000bytes-20db", 6, scenario, 1000, 20, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS6-LDPC-1000bytes-24db", 6, scenario, 1000, 24, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS6-LDPC-1000bytes-28db", 6, scenario, 1000, 28, 40000), TestCase::QUICK);

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS7-LDPC-1000bytes-18db", 7, scenario, 1000, 18, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS7-LDPC-1000bytes-22db", 7, scenario, 1000, 22, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS7-LDPC-1000bytes-26db", 7, scenario, 1000, 26, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS7-LDPC-1000bytes-30db", 7, scenario, 1000, 30, 40000), TestCase::QUICK);

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS8-LDPC-1000bytes-21.5db", 8, scenario, 1000, 21.5, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS8-LDPC-1000bytes-25.5db", 8, scenario, 1000, 25.5, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS8-LDPC-1000bytes-29.5db", 8, scenario, 1000, 29.5, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS8-LDPC-1000bytes-33.5db", 8, scenario, 1000, 33.5, 40000), TestCase::QUICK);

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS9-LDPC-1000bytes-20db", 9, scenario, 1000, 20, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS9-LDPC-1000bytes-24db", 9, scenario, 1000, 24, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS9-LDPC-1000bytes-28db", 9, scenario, 1000, 28, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS9-LDPC-1000bytes-32db", 9, scenario, 1000, 32, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS9-LDPC-1000bytes-36db", 9, scenario, 1000, 36, 40000), TestCase::QUICK);

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS10-LDPC-1000bytes-21.5db", 10, scenario, 1000, 21.5, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS10-LDPC-1000bytes-25.5db", 10, scenario, 1000, 25.5, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS10-LDPC-1000bytes-29.5db", 10, scenario, 1000, 29.5, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS10-LDPC-1000bytes-33.5db", 10, scenario, 1000, 33.5, 40000), TestCase::QUICK);

      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS11-LDPC-1000bytes-22db", 11, scenario, 1000, 22, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS11-LDPC-1000bytes-26db", 11, scenario, 1000, 26, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS11-LDPC-1000bytes-30db", 11, scenario, 1000, 30, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS11-LDPC-1000bytes-34db", 11, scenario, 1000, 34, 40000), TestCase::QUICK);
      AddTestCase (new LogSgnErrorRateOfdmaPerTestCase ("LogSgn-11ax-OFDMA-SISO-MCS11-LDPC-1000bytes-38db", 11, scenario, 1000, 38, 40000), TestCase::QUICK);
    }
}

static WifiLogSgnPerTestSuite wifiLogSgnPerTestSuite; ///< the test suite
