ns-3 Wi-Fi link-to-system mapping error models
==============================================

This module contains the LogSgnErrorRateModel implementation described in [[1]](#1).

## Usage

This module can be downloaded for ns-3 for release 3.36.1 or later.  The
module can be downloaded into the `contrib` directory.

To use the LogSgnErrorRateModel, specify `ns3::LogSgnErrorRateModel` as
the ErrorModel for the SpectrumWifiPhy instance or helper; e.g.:

```
    SpectrumWifiPhyHelper spectrumPhy;
    spectrumPhy.SetErrorRateModel ("ns3::LogSgnErrorRateModel");
```

The `wifi-l2s-error-models` module must be linked by the ns-3 program, or
else the program must be in the `scratch` directory.

At present, there is an attribute `ChannelModelType` to specify the channel
model type, but only one value (channel model D) is supported by the table data.

## References
<a id="1">[1]</a>
Sian Jin, Sumit Roy, Weihua Jiang, and Thomas R. Henderson (2020).
Efficient Abstractions for Implementing TGn Channel and OFDM-MIMO Links in ns-3.
In Proceedings of the 2020 Workshop on ns-3 (WNS3 2020).
Association for Computing Machinery, New York, NY, USA, 33–40.

