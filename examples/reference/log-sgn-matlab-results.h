/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Sébastien Deronne <sebastien.deronne@gmail.com>
 *          Sian Jin <sianjin@uw.edu>
 *          Jun Hyeon Park <pigncat@uw.edu>
 */

#ifndef LOG_SGN_MATLAB_RESULTS_H
#define LOG_SGN_MATLAB_RESULTS_H

namespace ns3 {

typedef std::map<std::tuple<uint8_t /*mcs*/, uint16_t /*width*/, double /*snr*/>, double /*per*/> LogSgnMatlabResults;

static const LogSgnMatlabResults fullPhySisoResults =
{
    //MCS-0 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (0, 2, -3.00), 0.8406},
    {std::make_tuple (0, 2, 0.00), 0.5959},
    {std::make_tuple (0, 2, 3.00), 0.3628},
    {std::make_tuple (0, 2, 6.00), 0.2032},
    {std::make_tuple (0, 2, 9.00), 0.1031},

    //MCS-1 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (1, 2, 0.00), 0.8330},
    {std::make_tuple (1, 2, 4.00), 0.5108},
    {std::make_tuple (1, 2, 8.00), 0.2439},
    {std::make_tuple (1, 2, 12.00), 0.0991},

    //MCS-2 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (2, 2, 2.00), 0.8730},
    {std::make_tuple (2, 2, 6.00), 0.5662},
    {std::make_tuple (2, 2, 10.00), 0.2954},
    {std::make_tuple (2, 2, 14.00), 0.1373},
    {std::make_tuple (2, 2, 18.00), 0.0577},

    //MCS-3 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (3, 2, 8.00), 0.6639},
    {std::make_tuple (3, 2, 12.00), 0.3501},
    {std::make_tuple (3, 2, 16.00), 0.1618},
    {std::make_tuple (3, 2, 20.00), 0.0606},

    //MCS-4 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (4, 2, 11.00), 0.6793},
    {std::make_tuple (4, 2, 15.00), 0.3707},
    {std::make_tuple (4, 2, 19.00), 0.1750},
    {std::make_tuple (4, 2, 22.00), 0.0973},

    //MCS-5 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (5, 2, 14.00), 0.7453},
    {std::make_tuple (5, 2, 18.00), 0.4259},
    {std::make_tuple (5, 2, 22.00), 0.2031},
    {std::make_tuple (5, 2, 26.00), 0.0886},
    {std::make_tuple (5, 2, 28.00), 0.0544},

    //MCS-6 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (6, 2, 16.00), 0.6938},
    {std::make_tuple (6, 2, 20.00), 0.3833},
    {std::make_tuple (6, 2, 24.00), 0.1802},
    {std::make_tuple (6, 2, 28.00), 0.0832},

    //MCS-7 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (7, 2, 18.00), 0.6518},
    {std::make_tuple (7, 2, 22.00), 0.3539},
    {std::make_tuple (7, 2, 26.00), 0.1739},
    {std::make_tuple (7, 2, 30.00), 0.0848},

    //MCS-8 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (8, 2, 21.50), 0.6664},
    {std::make_tuple (8, 2, 25.50), 0.3675},
    {std::make_tuple (8, 2, 29.50), 0.1784},
    {std::make_tuple (8, 2, 33.50), 0.0925},

    //MCS-9 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (9, 2, 20.00), 0.8961},
    {std::make_tuple (9, 2, 24.00), 0.6037},
    {std::make_tuple (9, 2, 28.00), 0.3297},
    {std::make_tuple (9, 2, 32.00), 0.1666},
    {std::make_tuple (9, 2, 36.00), 0.0970},

    //MCS-10 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (10, 2, 21.50), 0.9560},
    {std::make_tuple (10, 2, 25.50), 0.7233},
    {std::make_tuple (10, 2, 29.50), 0.4140},
    {std::make_tuple (10, 2, 33.50), 0.2305},
    {std::make_tuple (10, 2, 37.50), 0.1333},

    //MCS-11 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (11, 2, 22.00), 0.9873},
    {std::make_tuple (11, 2, 26.00), 0.8348},
    {std::make_tuple (11, 2, 30.00), 0.5510},
    {std::make_tuple (11, 2, 34.00), 0.3088},
    {std::make_tuple (11, 2, 38.00), 0.1838},

    //MCS-0 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (0, 4, -3.00), 0.8400},
    {std::make_tuple (0, 4, 0.00), 0.6030},
    {std::make_tuple (0, 4, 3.00), 0.3543},
    {std::make_tuple (0, 4, 6.00), 0.1803},
    {std::make_tuple (0, 4, 9.00), 0.0752},

    //MCS-1 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (1, 4, 0.00), 0.8382},
    {std::make_tuple (1, 4, 4.00), 0.5063},
    {std::make_tuple (1, 4, 8.00), 0.2244},
    {std::make_tuple (1, 4, 12.00), 0.0723},

    //MCS-2 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (2, 4, 2.00), 0.8856},
    {std::make_tuple (2, 4, 6.00), 0.5903},
    {std::make_tuple (2, 4, 10.00), 0.3074},
    {std::make_tuple (2, 4, 14.00), 0.1357},
    {std::make_tuple (2, 4, 18.00), 0.0431},

    //MCS-3 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (3, 4, 8.00), 0.6746},
    {std::make_tuple (3, 4, 12.00), 0.3473},
    {std::make_tuple (3, 4, 16.00), 0.1338},
    {std::make_tuple (3, 4, 20.00), 0.0381},

    //MCS-4 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (4, 4, 11.00), 0.6944},
    {std::make_tuple (4, 4, 15.00), 0.3896},
    {std::make_tuple (4, 4, 19.00), 0.1757},
    {std::make_tuple (4, 4, 22.00), 0.0851},

    //MCS-5 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (5, 4, 14.00), 0.7489},
    {std::make_tuple (5, 4, 18.00), 0.4255},
    {std::make_tuple (5, 4, 22.00), 0.1872},
    {std::make_tuple (5, 4, 26.00), 0.0619},
    {std::make_tuple (5, 4, 28.00), 0.0312},

    //MCS-6 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (6, 4, 16.00), 0.7078},
    {std::make_tuple (6, 4, 20.00), 0.3943},
    {std::make_tuple (6, 4, 24.00), 0.1717},
    {std::make_tuple (6, 4, 28.00), 0.0618},

    //MCS-7 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (7, 4, 18.00), 0.6727},
    {std::make_tuple (7, 4, 22.00), 0.3728},
    {std::make_tuple (7, 4, 26.00), 0.1779},
    {std::make_tuple (7, 4, 30.00), 0.0691},

    //MCS-8 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (8, 4, 21.50), 0.6724},
    {std::make_tuple (8, 4, 25.50), 0.3607},
    {std::make_tuple (8, 4, 29.50), 0.1528},
    {std::make_tuple (8, 4, 33.50), 0.0549},
    {std::make_tuple (8, 4, 37.50), 0.0169},

    //MCS-9 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (9, 4, 20.00), 0.8996},
    {std::make_tuple (9, 4, 24.00), 0.6109},
    {std::make_tuple (9, 4, 28.00), 0.3273},
    {std::make_tuple (9, 4, 32.00), 0.1494},
    {std::make_tuple (9, 4, 36.00), 0.0577},

    //MCS-10 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (10, 4, 21.50), 0.9653},
    {std::make_tuple (10, 4, 25.50), 0.7493},
    {std::make_tuple (10, 4, 29.50), 0.4365},
    {std::make_tuple (10, 4, 33.50), 0.2018},
    {std::make_tuple (10, 4, 37.50), 0.0793},

    //MCS-11 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (11, 4, 22.00), 0.9903},
    {std::make_tuple (11, 4, 26.00), 0.8510},
    {std::make_tuple (11, 4, 30.00), 0.5478},
    {std::make_tuple (11, 4, 34.00), 0.2805},
    {std::make_tuple (11, 4, 38.00), 0.1403},

    //MCS-0 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (0, 8, -3.00), 0.8769},
    {std::make_tuple (0, 8, 0.00), 0.6287},
    {std::make_tuple (0, 8, 3.00), 0.3326},
    {std::make_tuple (0, 8, 6.00), 0.1353},
    {std::make_tuple (0, 8, 9.00), 0.0420},

    //MCS-1 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (1, 8, 0.00), 0.8738},
    {std::make_tuple (1, 8, 4.00), 0.5226},
    {std::make_tuple (1, 8, 8.00), 0.1859},
    {std::make_tuple (1, 8, 12.00), 0.0395},

    //MCS-2 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (2, 8, 2.00), 0.9266},
    {std::make_tuple (2, 8, 6.00), 0.6570},
    {std::make_tuple (2, 8, 10.00), 0.3251},
    {std::make_tuple (2, 8, 14.00), 0.1128},
    {std::make_tuple (2, 8, 18.00), 0.0256},

    //MCS-3 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (3, 8, 8.00), 0.7137},
    {std::make_tuple (3, 8, 12.00), 0.3313},
    {std::make_tuple (3, 8, 16.00), 0.0931},
    {std::make_tuple (3, 8, 20.00), 0.0163},

    //MCS-4 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (4, 8, 11.00), 0.7583},
    {std::make_tuple (4, 8, 15.00), 0.4227},
    {std::make_tuple (4, 8, 19.00), 0.1600},
    {std::make_tuple (4, 8, 22.00), 0.0601},

    //MCS-5 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (5, 8, 14.00), 0.7981},
    {std::make_tuple (5, 8, 18.00), 0.4454},
    {std::make_tuple (5, 8, 22.00), 0.1586},
    {std::make_tuple (5, 8, 26.00), 0.0363},
    {std::make_tuple (5, 8, 28.00), 0.0158},

    //MCS-6 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (6, 8, 16.00), 0.7657},
    {std::make_tuple (6, 8, 20.00), 0.4169},
    {std::make_tuple (6, 8, 24.00), 0.1470},
    {std::make_tuple (6, 8, 28.00), 0.0372},

    //MCS-7 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (7, 8, 18.00), 0.7484},
    {std::make_tuple (7, 8, 22.00), 0.4277},
    {std::make_tuple (7, 8, 26.00), 0.1740},
    {std::make_tuple (7, 8, 30.00), 0.0503},

    //MCS-8 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (8, 8, 21.50), 0.7267},
    {std::make_tuple (8, 8, 25.50), 0.3717},
    {std::make_tuple (8, 8, 29.50), 0.1261},
    {std::make_tuple (8, 8, 33.50), 0.0313},
    {std::make_tuple (8, 8, 37.50), 0.0079},

    //MCS-9 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (9, 8, 20.00), 0.9430},
    {std::make_tuple (9, 8, 24.00), 0.6921},
    {std::make_tuple (9, 8, 28.00), 0.3650},
    {std::make_tuple (9, 8, 32.00), 0.1371},
    {std::make_tuple (9, 8, 36.00), 0.0423},

    //MCS-10 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (10, 8, 21.50), 0.9865},
    {std::make_tuple (10, 8, 25.50), 0.8000},
    {std::make_tuple (10, 8, 29.50), 0.4643},
    {std::make_tuple (10, 8, 33.50), 0.1675},
    {std::make_tuple (10, 8, 37.50), 0.0505},

    //MCS-11 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (11, 8, 22.00), 0.9978},
    {std::make_tuple (11, 8, 26.00), 0.9148},
    {std::make_tuple (11, 8, 30.00), 0.6345},
    {std::make_tuple (11, 8, 34.00), 0.3185},
    {std::make_tuple (11, 8, 38.00), 0.1148},

    //MCS-0 20MHz
    {std::make_tuple (0, 20, -3.00), 0.9357},
    {std::make_tuple (0, 20, -2.00), 0.8779},
    {std::make_tuple (0, 20, -1.00), 0.7889},
    {std::make_tuple (0, 20, 0.00), 0.6748},
    {std::make_tuple (0, 20, 1.00), 0.5443},
    {std::make_tuple (0, 20, 2.00), 0.4163},
    {std::make_tuple (0, 20, 3.00), 0.2946},
    {std::make_tuple (0, 20, 4.00), 0.1995},
    {std::make_tuple (0, 20, 5.00), 0.1257},
    {std::make_tuple (0, 20, 6.00), 0.0750},
    {std::make_tuple (0, 20, 7.00), 0.0407},
    {std::make_tuple (0, 20, 8.00), 0.0198},
    {std::make_tuple (0, 20, 9.00), 0.0110},
    {std::make_tuple (0, 20, 10.00), 0.0045},

    //MCS-1 20MHz
    {std::make_tuple (1, 20, 0.00), 0.9335},
    {std::make_tuple (1, 20, 4.00), 0.5292},
    {std::make_tuple (1, 20, 8.00), 0.1177},
    {std::make_tuple (1, 20, 12.00), 0.0093},

    //MCS-2 20MHz
    {std::make_tuple (2, 20, 2.00), 0.9781},
    {std::make_tuple (2, 20, 6.00), 0.7543},
    {std::make_tuple (2, 20, 10.00), 0.3244},
    {std::make_tuple (2, 20, 14.00), 0.0694},
    {std::make_tuple (2, 20, 18.00), 0.0074},

    //MCS-3 20MHz
    {std::make_tuple (3, 20, 8.00), 0.7735},
    {std::make_tuple (3, 20, 12.00), 0.2786},
    {std::make_tuple (3, 20, 16.00), 0.0380},
    {std::make_tuple (3, 20, 20.00), 0.0018},

    //MCS-4 20MHz
    {std::make_tuple (4, 20, 11.00), 0.8531},
    {std::make_tuple (4, 20, 15.00), 0.4345},
    {std::make_tuple (4, 20, 19.00), 0.1034},
    {std::make_tuple (4, 20, 23.00), 0.0116},

    //MCS-5 20MHz
    {std::make_tuple (5, 20, 14.00), 0.8867},
    {std::make_tuple (5, 20, 18.00), 0.4561},
    {std::make_tuple (5, 20, 22.00), 0.0968},
    {std::make_tuple (5, 20, 26.00), 0.0081},
    {std::make_tuple (5, 20, 28.00), 0.0017},

    //MCS-6 20MHz
    {std::make_tuple (6, 20, 16.00), 0.8552},
    {std::make_tuple (6, 20, 20.00), 0.4232},
    {std::make_tuple (6, 20, 24.00), 0.0960},
    {std::make_tuple (6, 20, 28.00), 0.0090},

    //MCS-7 20MHz
    {std::make_tuple (7, 20, 18.00), 0.7936},
    {std::make_tuple (7, 20, 22.00), 0.3665},
    {std::make_tuple (7, 20, 26.00), 0.0831},
    {std::make_tuple (7, 20, 30.00), 0.0090},

    //MCS-8 20MHz
    {std::make_tuple (8, 20, 21.50), 0.8125},
    {std::make_tuple (8, 20, 25.50), 0.3546},
    {std::make_tuple (8, 20, 29.50), 0.0648},
    {std::make_tuple (8, 20, 33.50), 0.0056},

    //MCS-9 20MHz
    {std::make_tuple (9, 20, 20.00), 0.9663},
    {std::make_tuple (9, 20, 24.00), 0.7005},
    {std::make_tuple (9, 20, 28.00), 0.2635},
    {std::make_tuple (9, 20, 32.00), 0.0453},
    {std::make_tuple (9, 20, 36.00), 0.0038},

    //MCS-10 20MHz
    {std::make_tuple (10, 20, 21.50), 0.9972},
    {std::make_tuple (10, 20, 23.50), 0.9733},
    {std::make_tuple (10, 20, 25.50), 0.8821},
    {std::make_tuple (10, 20, 27.50), 0.6931},
    {std::make_tuple (10, 20, 29.50), 0.4484},
    {std::make_tuple (10, 20, 31.50), 0.2300},
    {std::make_tuple (10, 20, 32.00), 0.1888},
    {std::make_tuple (10, 20, 32.50), 0.1535},
    {std::make_tuple (10, 20, 33.00), 0.1224},
    {std::make_tuple (10, 20, 33.50), 0.0953},
    {std::make_tuple (10, 20, 34.00), 0.0730},
    {std::make_tuple (10, 20, 34.50), 0.0560},

    //MCS-11 20MHz
    {std::make_tuple (11, 20, 22.00), 0.9993},
    {std::make_tuple (11, 20, 26.00), 0.9414},
    {std::make_tuple (11, 20, 30.00), 0.6046},
    {std::make_tuple (11, 20, 34.00), 0.1866},
    {std::make_tuple (11, 20, 34.50), 0.1509},
    {std::make_tuple (11, 20, 35.00), 0.1239},
    {std::make_tuple (11, 20, 35.50), 0.0940},
    {std::make_tuple (11, 20, 36.00), 0.0791},
    {std::make_tuple (11, 20, 36.50), 0.0596},
    {std::make_tuple (11, 20, 37.00), 0.0455},
    {std::make_tuple (11, 20, 37.50), 0.0352},
    {std::make_tuple (11, 20, 38.00), 0.0265},

    //MCS-0 40MHz
    {std::make_tuple (0, 40, -1.50), 0.9038},
    {std::make_tuple (0, 40, 1.50), 0.4800},
    {std::make_tuple (0, 40, 3.00), 0.2510},
    {std::make_tuple (0, 40, 4.50), 0.0999},
    {std::make_tuple (0, 40, 6.00), 0.0310},
    {std::make_tuple (0, 40, 7.00), 0.0118},

    //MCS-1 40MHz
    {std::make_tuple (1, 40, 0.00), 0.9846},
    {std::make_tuple (1, 40, 4.00), 0.6409},
    {std::make_tuple (1, 40, 6.00), 0.3248},
    {std::make_tuple (1, 40, 8.00), 0.1115},
    {std::make_tuple (1, 40, 10.00), 0.0245},
    {std::make_tuple (1, 40, 12.00), 0.0036},

    //MCS-2 40MHz
    {std::make_tuple (2, 40, 2.00), 0.9962},
    {std::make_tuple (2, 40, 6.00), 0.8374},
    {std::make_tuple (2, 40, 10.00), 0.2965},
    {std::make_tuple (2, 40, 12.50), 0.0819},
    {std::make_tuple (2, 40, 13.00), 0.0599},
    {std::make_tuple (2, 40, 13.50), 0.0396},
    {std::make_tuple (2, 40, 14.00), 0.0287},
    {std::make_tuple (2, 40, 14.50), 0.0192},
    {std::make_tuple (2, 40, 15.00), 0.0124},
    {std::make_tuple (2, 40, 15.50), 0.0082},

    //MCS-3 40MHz
    {std::make_tuple (3, 40, 8.00), 0.8416},
    {std::make_tuple (3, 40, 12.00), 0.2313},
    {std::make_tuple (3, 40, 14.00), 0.0586},
    {std::make_tuple (3, 40, 14.50), 0.0410},
    {std::make_tuple (3, 40, 15.00), 0.0248},
    {std::make_tuple (3, 40, 15.50), 0.0156},
    {std::make_tuple (3, 40, 16.00), 0.0095},
    {std::make_tuple (3, 40, 16.50), 0.0058},
    {std::make_tuple (3, 40, 17.00), 0.0030},
    {std::make_tuple (3, 40, 17.50), 0.0022},
    {std::make_tuple (3, 40, 18.00), 0.0011},

    //MCS-4 40MHz
    {std::make_tuple (4, 40, 11.00), 0.9272},
    {std::make_tuple (4, 40, 15.00), 0.4501},
    {std::make_tuple (4, 40, 17.00), 0.1936},
    {std::make_tuple (4, 40, 19.00), 0.0593},
    {std::make_tuple (4, 40, 19.50), 0.0419},
    {std::make_tuple (4, 40, 20.00), 0.0283},
    {std::make_tuple (4, 40, 20.50), 0.0183},
    {std::make_tuple (4, 40, 21.00), 0.0120},

    //MCS-5 40MHz
    {std::make_tuple (5, 40, 14.00), 0.9457},
    {std::make_tuple (5, 40, 18.00), 0.4496},
    {std::make_tuple (5, 40, 22.00), 0.0456},
    {std::make_tuple (5, 40, 22.50), 0.0298},
    {std::make_tuple (5, 40, 23.00), 0.0196},
    {std::make_tuple (5, 40, 23.50), 0.0116},
    {std::make_tuple (5, 40, 24.00), 0.0082},
    {std::make_tuple (5, 40, 24.50), 0.0055},
    {std::make_tuple (5, 40, 25.00), 0.0031},

    //MCS-6 40MHz
    {std::make_tuple (6, 40, 16.00), 0.9224},
    {std::make_tuple (6, 40, 20.00), 0.4178},
    {std::make_tuple (6, 40, 22.00), 0.1692},
    {std::make_tuple (6, 40, 24.00), 0.0463},
    {std::make_tuple (6, 40, 26.00), 0.0096},
    {std::make_tuple (6, 40, 28.00), 0.0011},

    //MCS-7 40MHz
    {std::make_tuple (7, 40, 18.00), 0.8817},
    {std::make_tuple (7, 40, 22.00), 0.3743},
    {std::make_tuple (7, 40, 24.00), 0.1508},
    {std::make_tuple (7, 40, 26.00), 0.0421},
    {std::make_tuple (7, 40, 28.00), 0.0093},
    {std::make_tuple (7, 40, 30.00), 0.0013},

    //MCS-8 40MHz
    {std::make_tuple (8, 40, 21.50), 0.8792},
    {std::make_tuple (8, 40, 25.50), 0.3228},
    {std::make_tuple (8, 40, 28.00), 0.0787},
    {std::make_tuple (8, 40, 28.50), 0.0561},
    {std::make_tuple (8, 40, 29.00), 0.0368},
    {std::make_tuple (8, 40, 29.50), 0.0237},
    {std::make_tuple (8, 40, 30.00), 0.0159},
    {std::make_tuple (8, 40, 30.50), 0.0100},
    {std::make_tuple (8, 40, 31.00), 0.0063},

    //MCS-9 40MHz
    {std::make_tuple (9, 40, 20.00), 0.9953},
    {std::make_tuple (9, 40, 24.00), 0.8085},
    {std::make_tuple (9, 40, 28.00), 0.2539},
    {std::make_tuple (9, 40, 30.00), 0.0882},
    {std::make_tuple (9, 40, 30.50), 0.0634},
    {std::make_tuple (9, 40, 31.00), 0.0427},
    {std::make_tuple (9, 40, 31.50), 0.0302},
    {std::make_tuple (9, 40, 32.00), 0.0201},
    {std::make_tuple (9, 40, 32.50), 0.0131},
    {std::make_tuple (9, 40, 33.00), 0.0094},

    //MCS-10 40MHz
    {std::make_tuple (10, 40, 21.50), 0.9995},
    {std::make_tuple (10, 40, 23.50), 0.9880},
    {std::make_tuple (10, 40, 25.50), 0.9019},
    {std::make_tuple (10, 40, 27.50), 0.6641},
    {std::make_tuple (10, 40, 29.50), 0.3456},
    {std::make_tuple (10, 40, 31.50), 0.1144},
    {std::make_tuple (10, 40, 32.00), 0.0829},
    {std::make_tuple (10, 40, 32.50), 0.0564},
    {std::make_tuple (10, 40, 33.00), 0.0384},
    {std::make_tuple (10, 40, 33.50), 0.0254},
    {std::make_tuple (10, 40, 34.00), 0.0161},
    {std::make_tuple (10, 40, 34.50), 0.0101},

    //MCS-11 40MHz
    {std::make_tuple (11, 40, 22.00), 1.0000},
    {std::make_tuple (11, 40, 26.00), 0.9941},
    {std::make_tuple (11, 40, 30.00), 0.7835},
    {std::make_tuple (11, 40, 34.00), 0.2293},
    {std::make_tuple (11, 40, 34.50), 0.1769},
    {std::make_tuple (11, 40, 35.00), 0.1370},
    {std::make_tuple (11, 40, 35.50), 0.1030},
    {std::make_tuple (11, 40, 36.00), 0.0741},
    {std::make_tuple (11, 40, 36.50), 0.0511},
    {std::make_tuple (11, 40, 37.00), 0.0373},
    {std::make_tuple (11, 40, 37.50), 0.0260},
    {std::make_tuple (11, 40, 38.00), 0.0170},

    //MCS-0 80MHz
    {std::make_tuple (0, 80, -1.50), 0.9590},
    {std::make_tuple (0, 80, 1.50), 0.4630},
    {std::make_tuple (0, 80, 2.00), 0.3509},
    {std::make_tuple (0, 80, 2.50), 0.2521},
    {std::make_tuple (0, 80, 3.00), 0.1654},
    {std::make_tuple (0, 80, 3.50), 0.1059},
    {std::make_tuple (0, 80, 4.00), 0.0635},
    {std::make_tuple (0, 80, 4.50), 0.0360},
    {std::make_tuple (0, 80, 5.00), 0.0182},
    {std::make_tuple (0, 80, 5.50), 0.0090},

    //MCS-1 80MHz
    {std::make_tuple (1, 80, 0.00), 0.9968},
    {std::make_tuple (1, 80, 4.00), 0.5863},
    {std::make_tuple (1, 80, 6.00), 0.1712},
    {std::make_tuple (1, 80, 6.50), 0.1112},
    {std::make_tuple (1, 80, 7.00), 0.0654},
    {std::make_tuple (1, 80, 7.50), 0.0366},
    {std::make_tuple (1, 80, 8.00), 0.0203},
    {std::make_tuple (1, 80, 8.50), 0.0101},
    {std::make_tuple (1, 80, 9.00), 0.0050},

    //MCS-2 80MHz
    {std::make_tuple (2, 80, 2.00), 1.0000},
    {std::make_tuple (2, 80, 6.00), 0.9288},
    {std::make_tuple (2, 80, 10.00), 0.2678},
    {std::make_tuple (2, 80, 11.00), 0.1328},
    {std::make_tuple (2, 80, 11.50), 0.0864},
    {std::make_tuple (2, 80, 12.00), 0.0560},
    {std::make_tuple (2, 80, 12.50), 0.0339},
    {std::make_tuple (2, 80, 13.00), 0.0193},
    {std::make_tuple (2, 80, 13.50), 0.0105},
    {std::make_tuple (2, 80, 14.00), 0.0060},

    //MCS-3 80MHz
    {std::make_tuple (3, 80, 8.00), 0.9179},
    {std::make_tuple (3, 80, 10.00), 0.5629},
    {std::make_tuple (3, 80, 11.00), 0.3294},
    {std::make_tuple (3, 80, 12.00), 0.1572},
    {std::make_tuple (3, 80, 12.50), 0.0967},
    {std::make_tuple (3, 80, 13.00), 0.0582},
    {std::make_tuple (3, 80, 13.50), 0.0316},
    {std::make_tuple (3, 80, 14.00), 0.0168},
    {std::make_tuple (3, 80, 14.50), 0.0092},

    //MCS-4 80MHz
    {std::make_tuple (4, 80, 11.00), 0.9834},
    {std::make_tuple (4, 80, 15.00), 0.4754},
    {std::make_tuple (4, 80, 16.00), 0.2893},
    {std::make_tuple (4, 80, 17.00), 0.1482},
    {std::make_tuple (4, 80, 17.50), 0.0983},
    {std::make_tuple (4, 80, 18.00), 0.0625},
    {std::make_tuple (4, 80, 18.50), 0.0386},
    {std::make_tuple (4, 80, 19.00), 0.0232},
    {std::make_tuple (4, 80, 19.50), 0.0127},
    {std::make_tuple (4, 80, 20.00), 0.0065},

    //MCS-5 80MHz
    {std::make_tuple (5, 80, 14.00), 0.9860},
    {std::make_tuple (5, 80, 18.00), 0.4544},
    {std::make_tuple (5, 80, 19.00), 0.2603},
    {std::make_tuple (5, 80, 19.50), 0.1778},
    {std::make_tuple (5, 80, 20.00), 0.1176},
    {std::make_tuple (5, 80, 20.50), 0.0717},
    {std::make_tuple (5, 80, 21.00), 0.0453},
    {std::make_tuple (5, 80, 21.50), 0.0235},
    {std::make_tuple (5, 80, 22.00), 0.0132},

    //MCS-6 80MHz
    {std::make_tuple (6, 80, 16.00), 0.9580},
    {std::make_tuple (6, 80, 20.00), 0.2961},
    {std::make_tuple (6, 80, 20.50), 0.2154},
    {std::make_tuple (6, 80, 21.00), 0.1452},
    {std::make_tuple (6, 80, 21.50), 0.0950},
    {std::make_tuple (6, 80, 22.00), 0.0589},
    {std::make_tuple (6, 80, 22.50), 0.0354},
    {std::make_tuple (6, 80, 23.00), 0.0181},
    {std::make_tuple (6, 80, 23.50), 0.0092},
    {std::make_tuple (6, 80, 24.00), 0.0060},

    //MCS-7 80MHz
    {std::make_tuple (7, 80, 18.00), 0.9662},
    {std::make_tuple (7, 80, 22.00), 0.3953},
    {std::make_tuple (7, 80, 23.00), 0.2307},
    {std::make_tuple (7, 80, 23.50), 0.1615},
    {std::make_tuple (7, 80, 24.00), 0.1135},
    {std::make_tuple (7, 80, 24.50), 0.0731},
    {std::make_tuple (7, 80, 25.00), 0.0481},
    {std::make_tuple (7, 80, 25.50), 0.0294},
    {std::make_tuple (7, 80, 26.00), 0.0166},
    {std::make_tuple (7, 80, 26.50), 0.0087},

    //MCS-8 80MHz
    {std::make_tuple (8, 80, 21.50), 0.9077},
    {std::make_tuple (8, 80, 23.00), 0.6732},
    {std::make_tuple (8, 80, 24.50), 0.3453},
    {std::make_tuple (8, 80, 25.50), 0.1733},
    {std::make_tuple (8, 80, 26.00), 0.1165},
    {std::make_tuple (8, 80, 26.50), 0.0738},
    {std::make_tuple (8, 80, 27.00), 0.0429},
    {std::make_tuple (8, 80, 27.50), 0.0242},

    //MCS-9 80MHz
    {std::make_tuple (9, 80, 20.00), 0.9999},
    {std::make_tuple (9, 80, 24.00), 0.8864},
    {std::make_tuple (9, 80, 26.00), 0.5518},
    {std::make_tuple (9, 80, 28.00), 0.1819},
    {std::make_tuple (9, 80, 28.50), 0.1240},
    {std::make_tuple (9, 80, 29.00), 0.0801},
    {std::make_tuple (9, 80, 29.50), 0.0511},
    {std::make_tuple (9, 80, 30.00), 0.0308},
    {std::make_tuple (9, 80, 30.50), 0.0168},

    //MCS-10 80MHz
    {std::make_tuple (10, 80, 21.50), 1.0000},
    {std::make_tuple (10, 80, 23.50), 0.9996},
    {std::make_tuple (10, 80, 25.50), 0.9769},
    {std::make_tuple (10, 80, 27.50), 0.7864},
    {std::make_tuple (10, 80, 29.50), 0.3639},
    {std::make_tuple (10, 80, 31.50), 0.0756},
    {std::make_tuple (10, 80, 32.00), 0.0445},
    {std::make_tuple (10, 80, 32.50), 0.0256},
    {std::make_tuple (10, 80, 33.00), 0.0134},
    {std::make_tuple (10, 80, 33.50), 0.0067},
    {std::make_tuple (10, 80, 34.00), 0.0038},
    {std::make_tuple (10, 80, 34.50), 0.0018},

    //MCS-11 80MHz
    {std::make_tuple (11, 80, 22.00), 1.0000},
    {std::make_tuple (11, 80, 26.00), 0.9985},
    {std::make_tuple (11, 80, 30.00), 0.7525},
    {std::make_tuple (11, 80, 34.00), 0.0825},
    {std::make_tuple (11, 80, 34.50), 0.0498},
    {std::make_tuple (11, 80, 35.00), 0.0314},
    {std::make_tuple (11, 80, 35.50), 0.0175},
    {std::make_tuple (11, 80, 36.00), 0.0083},
    {std::make_tuple (11, 80, 36.50), 0.0046},
    {std::make_tuple (11, 80, 37.00), 0.0023},
    {std::make_tuple (11, 80, 37.50), 0.0008},
    {std::make_tuple (11, 80, 38.00), 0.0003},

    //MCS-0 160MHz
    {std::make_tuple (0, 160, -1.50), 0.9936},
    {std::make_tuple (0, 160, 1.50), 0.4764},
    {std::make_tuple (0, 160, 2.00), 0.3278},
    {std::make_tuple (0, 160, 2.50), 0.1975},
    {std::make_tuple (0, 160, 3.50), 0.0519},
    {std::make_tuple (0, 160, 4.00), 0.0229},
    {std::make_tuple (0, 160, 5.00), 0.0034},
    {std::make_tuple (0, 160, 5.50), 0.0009},

    //MCS-1 160MHz
    {std::make_tuple (1, 160, 0.00), 1.0000},
    {std::make_tuple (1, 160, 4.00), 0.6171},
    {std::make_tuple (1, 160, 6.00), 0.0998},
    {std::make_tuple (1, 160, 6.50), 0.0475},
    {std::make_tuple (1, 160, 7.00), 0.0212},
    {std::make_tuple (1, 160, 7.50), 0.0082},
    {std::make_tuple (1, 160, 8.50), 0.0009},
    {std::make_tuple (1, 160, 9.00), 0.0001},

    //MCS-2 80MHz
    {std::make_tuple (2, 160, 6.00), 0.9818},
    {std::make_tuple (2, 160, 10.00), 0.2214},
    {std::make_tuple (2, 160, 11.00), 0.0751},
    {std::make_tuple (2, 160, 11.50), 0.0387},
    {std::make_tuple (2, 160, 12.00), 0.0188},
    {std::make_tuple (2, 160, 12.50), 0.0081},
    {std::make_tuple (2, 160, 13.00), 0.0033},
    {std::make_tuple (2, 160, 13.50), 0.0012},

    //MCS-3 160MHz
    {std::make_tuple (3, 160, 8.00), 0.9801},
    {std::make_tuple (3, 160, 10.00), 0.6547},
    {std::make_tuple (3, 160, 11.00), 0.3411},
    {std::make_tuple (3, 160, 12.00), 0.1177},
    {std::make_tuple (3, 160, 13.00), 0.0261},
    {std::make_tuple (3, 160, 13.50), 0.0103},
    {std::make_tuple (3, 160, 14.00), 0.0035},
    {std::make_tuple (3, 160, 14.50), 0.0012},

    //MCS-4 160MHz
    {std::make_tuple (4, 160, 11.00), 0.9940},
    {std::make_tuple (4, 160, 15.00), 0.3016},
    {std::make_tuple (4, 160, 17.00), 0.0299},
    {std::make_tuple (4, 160, 18.00), 0.0045},
    {std::make_tuple (4, 160, 18.50), 0.0019},
    {std::make_tuple (4, 160, 19.00), 0.0007},
    {std::make_tuple (4, 160, 19.50), 0.0002},
    {std::make_tuple (4, 160, 20.00), 0.0001},

    //MCS-5 160MHz
    {std::make_tuple (5, 160, 14.00), 0.9994},
    {std::make_tuple (5, 160, 18.00), 0.4865},
    {std::make_tuple (5, 160, 20.00), 0.0636},
    {std::make_tuple (5, 160, 20.50), 0.0289},
    {std::make_tuple (5, 160, 21.00), 0.0119},
    {std::make_tuple (5, 160, 21.50), 0.0046},
    {std::make_tuple (5, 160, 22.00), 0.0018},
    {std::make_tuple (5, 160, 22.50), 0.0004},

    //MCS-6 160MHz
    {std::make_tuple (6, 160, 16.00), 0.9921},
    {std::make_tuple (6, 160, 19.00), 0.5225},
    {std::make_tuple (6, 160, 20.50), 0.1520},
    {std::make_tuple (6, 160, 21.00), 0.0840},
    {std::make_tuple (6, 160, 21.50), 0.0395},
    {std::make_tuple (6, 160, 22.50), 0.0077},
    {std::make_tuple (6, 160, 23.00), 0.0033},
    {std::make_tuple (6, 160, 23.50), 0.0011},

    //MCS-7 160MHz
    {std::make_tuple (7, 160, 18.00), 0.9953},
    {std::make_tuple (7, 160, 22.00), 0.3980},
    {std::make_tuple (7, 160, 23.50), 0.1057},
    {std::make_tuple (7, 160, 24.50), 0.0306},
    {std::make_tuple (7, 160, 25.00), 0.0148},
    {std::make_tuple (7, 160, 25.50), 0.0067},
    {std::make_tuple (7, 160, 26.00), 0.0030},
    {std::make_tuple (7, 160, 26.50), 0.0009},

    //MCS-8 160MHz
    {std::make_tuple (8, 160, 21.00), 0.9908},
    {std::make_tuple (8, 160, 24.00), 0.4992},
    {std::make_tuple (8, 160, 26.00), 0.0699},
    {std::make_tuple (8, 160, 26.50), 0.0336},
    {std::make_tuple (8, 160, 27.00), 0.0143},
    {std::make_tuple (8, 160, 27.50), 0.0058},
    {std::make_tuple (8, 160, 28.00), 0.0022},
    {std::make_tuple (8, 160, 28.50), 0.0006},

    //MCS-9 160MHz
    {std::make_tuple (9, 160, 22.00), 0.9998},
    {std::make_tuple (9, 160, 26.00), 0.6640},
    {std::make_tuple (9, 160, 28.50), 0.0939},
    {std::make_tuple (9, 160, 29.00), 0.0514},
    {std::make_tuple (9, 160, 29.50), 0.0255},
    {std::make_tuple (9, 160, 30.00), 0.0119},
    {std::make_tuple (9, 160, 30.50), 0.0049},
    {std::make_tuple (9, 160, 31.00), 0.0021},

    //MCS-10 160MHz
    {std::make_tuple (10, 160, 21.50), 1.0000},
    {std::make_tuple (10, 160, 23.50), 1.0000},
    {std::make_tuple (10, 160, 25.50), 0.9996},
    {std::make_tuple (10, 160, 27.50), 0.9521},
    {std::make_tuple (10, 160, 29.50), 0.5362},
    {std::make_tuple (10, 160, 31.50), 0.0827},
    {std::make_tuple (10, 160, 32.00), 0.0406},
    {std::make_tuple (10, 160, 32.50), 0.0174},
    {std::make_tuple (10, 160, 33.00), 0.0073},
    {std::make_tuple (10, 160, 33.50), 0.0030},
    {std::make_tuple (10, 160, 34.00), 0.0009},
    {std::make_tuple (10, 160, 34.50), 0.0003},

    //MCS-11 160MHz
    {std::make_tuple (11, 160, 22.00), 1.0000},
    {std::make_tuple (11, 160, 26.00), 1.0000},
    {std::make_tuple (11, 160, 30.00), 0.8394},
    {std::make_tuple (11, 160, 34.00), 0.0357},
    {std::make_tuple (11, 160, 34.50), 0.0173},
    {std::make_tuple (11, 160, 35.00), 0.0073},
    {std::make_tuple (11, 160, 35.50), 0.0027},
    {std::make_tuple (11, 160, 36.00), 0.0014},
};

static const LogSgnMatlabResults logSgnSisoResults =
{
    //MCS-0 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (0, 2, -3.00), 0.8492},
    {std::make_tuple (0, 2, 0.00), 0.6346},
    {std::make_tuple (0, 2, 3.00), 0.3613},
    {std::make_tuple (0, 2, 6.00), 0.1831},
    {std::make_tuple (0, 2, 9.00), 0.0733},

    //MCS-1 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (1, 2, 0.00), 0.8486},
    {std::make_tuple (1, 2, 4.00), 0.5453},
    {std::make_tuple (1, 2, 8.00), 0.2276},
    {std::make_tuple (1, 2, 12.00), 0.0734},

    //MCS-2 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (2, 2, 2.00), 0.8784},
    {std::make_tuple (2, 2, 6.00), 0.6100},
    {std::make_tuple (2, 2, 10.00), 0.2877},
    {std::make_tuple (2, 2, 14.00), 0.1131},
    {std::make_tuple (2, 2, 18.00), 0.0352},

    //MCS-3 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (3, 2, 8.00), 0.6861},
    {std::make_tuple (3, 2, 12.00), 0.3447},
    {std::make_tuple (3, 2, 16.00), 0.1450},
    {std::make_tuple (3, 2, 20.00), 0.0442},

    //MCS-4 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (4, 2, 11.00), 0.7075},
    {std::make_tuple (4, 2, 15.00), 0.3672},
    {std::make_tuple (4, 2, 19.00), 0.1555},
    {std::make_tuple (4, 2, 22.00), 0.0759},

    //MCS-5 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (5, 2, 14.00), 0.7743},
    {std::make_tuple (5, 2, 18.00), 0.4432},
    {std::make_tuple (5, 2, 22.00), 0.1859},
    {std::make_tuple (5, 2, 26.00), 0.0606},
    {std::make_tuple (5, 2, 28.00), 0.0396},

    //MCS-6 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (6, 2, 16.00), 0.7319},
    {std::make_tuple (6, 2, 20.00), 0.3839},
    {std::make_tuple (6, 2, 24.00), 0.1565},
    {std::make_tuple (6, 2, 28.00), 0.0528},

    //MCS-7 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (7, 2, 18.00), 0.7011},
    {std::make_tuple (7, 2, 22.00), 0.3551},
    {std::make_tuple (7, 2, 26.00), 0.1445},
    {std::make_tuple (7, 2, 30.00), 0.0584},

    //MCS-8 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (8, 2, 21.50), 0.7076},
    {std::make_tuple (8, 2, 25.50), 0.3714},
    {std::make_tuple (8, 2, 29.50), 0.1501},
    {std::make_tuple (8, 2, 33.50), 0.0543},

    //MCS-9 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (9, 2, 20.00), 0.8993},
    {std::make_tuple (9, 2, 24.00), 0.6569},
    {std::make_tuple (9, 2, 28.00), 0.3226},
    {std::make_tuple (9, 2, 32.00), 0.1266},
    {std::make_tuple (9, 2, 36.00), 0.0448},

    //MCS-10 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (10, 2, 21.50), 0.9424},
    {std::make_tuple (10, 2, 25.50), 0.7546},
    {std::make_tuple (10, 2, 29.50), 0.4172},
    {std::make_tuple (10, 2, 33.50), 0.1765},
    {std::make_tuple (10, 2, 37.50), 0.0750},

    //MCS-11 2MHz (OFDMA - 26 subcarriers)
    {std::make_tuple (11, 2, 22.00), 0.9714},
    {std::make_tuple (11, 2, 26.00), 0.8498},
    {std::make_tuple (11, 2, 30.00), 0.5748},
    {std::make_tuple (11, 2, 34.00), 0.2615},
    {std::make_tuple (11, 2, 38.00), 0.0991},

    //MCS-0 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (0, 4, -3.00), 0.8741},
    {std::make_tuple (0, 4, 0.00), 0.6605},
    {std::make_tuple (0, 4, 3.00), 0.3484},
    {std::make_tuple (0, 4, 6.00), 0.1602},
    {std::make_tuple (0, 4, 9.00), 0.0577},

    //MCS-1 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (1, 4, 0.00), 0.8773},
    {std::make_tuple (1, 4, 4.00), 0.5602},
    {std::make_tuple (1, 4, 8.00), 0.2159},
    {std::make_tuple (1, 4, 12.00), 0.0527},

    //MCS-2 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (2, 4, 2.00), 0.9028},
    {std::make_tuple (2, 4, 6.00), 0.6202},
    {std::make_tuple (2, 4, 10.00), 0.2542},
    {std::make_tuple (2, 4, 14.00), 0.0674},
    {std::make_tuple (2, 4, 18.00), 0.0114},

    //MCS-3 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (3, 4, 8.00), 0.7108},
    {std::make_tuple (3, 4, 12.00), 0.3382},
    {std::make_tuple (3, 4, 16.00), 0.1064},
    {std::make_tuple (3, 4, 20.00), 0.0241},

    //MCS-4 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (4, 4, 11.00), 0.7365},
    {std::make_tuple (4, 4, 15.00), 0.3859},
    {std::make_tuple (4, 4, 19.00), 0.1511},
    {std::make_tuple (4, 4, 22.00), 0.0644},

    //MCS-5 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (5, 4, 14.00), 0.7971},
    {std::make_tuple (5, 4, 18.00), 0.4446},
    {std::make_tuple (5, 4, 22.00), 0.1620},
    {std::make_tuple (5, 4, 26.00), 0.0390},
    {std::make_tuple (5, 4, 28.00), 0.0198},

    //MCS-6 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (6, 4, 16.00), 0.7581},
    {std::make_tuple (6, 4, 20.00), 0.3932},
    {std::make_tuple (6, 4, 24.00), 0.1352},
    {std::make_tuple (6, 4, 28.00), 0.0385},

    //MCS-7 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (7, 4, 18.00), 0.7170},
    {std::make_tuple (7, 4, 22.00), 0.3449},
    {std::make_tuple (7, 4, 26.00), 0.1115},
    {std::make_tuple (7, 4, 30.00), 0.0231},

    //MCS-8 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (8, 4, 21.50), 0.7298},
    {std::make_tuple (8, 4, 25.50), 0.3532},
    {std::make_tuple (8, 4, 29.50), 0.1149},
    {std::make_tuple (8, 4, 33.50), 0.0244},
    {std::make_tuple (8, 4, 37.50), 0.0040},

    //MCS-9 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (9, 4, 20.00), 0.9239},
    {std::make_tuple (9, 4, 24.00), 0.6734},
    {std::make_tuple (9, 4, 28.00), 0.3080},
    {std::make_tuple (9, 4, 32.00), 0.0979},
    {std::make_tuple (9, 4, 36.00), 0.0192},

    //MCS-10 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (10, 4, 21.50), 0.9643},
    {std::make_tuple (10, 4, 25.50), 0.8119},
    {std::make_tuple (10, 4, 29.50), 0.4706},
    {std::make_tuple (10, 4, 33.50), 0.1832},
    {std::make_tuple (10, 4, 37.50), 0.0553},

    //MCS-11 4MHz (OFDMA - 52 subcarriers)
    {std::make_tuple (11, 4, 22.00), 0.9829},
    {std::make_tuple (11, 4, 26.00), 0.8929},
    {std::make_tuple (11, 4, 30.00), 0.5990},
    {std::make_tuple (11, 4, 34.00), 0.2437},
    {std::make_tuple (11, 4, 38.00), 0.0761},

    //MCS-0 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (0, 8, -3.00), 0.8910},
    {std::make_tuple (0, 8, 0.00), 0.6349},
    {std::make_tuple (0, 8, 3.00), 0.2889},
    {std::make_tuple (0, 8, 6.00), 0.0920},
    {std::make_tuple (0, 8, 9.00), 0.0179},

    //MCS-1 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (1, 8, 0.00), 0.8942},
    {std::make_tuple (1, 8, 4.00), 0.5246},
    {std::make_tuple (1, 8, 8.00), 0.1506},
    {std::make_tuple (1, 8, 12.00), 0.0238},

    //MCS-2 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (2, 8, 2.00), 0.9362},
    {std::make_tuple (2, 8, 6.00), 0.6890},
    {std::make_tuple (2, 8, 10.00), 0.3150},
    {std::make_tuple (2, 8, 14.00), 0.1008},
    {std::make_tuple (2, 8, 18.00), 0.0347},

    //MCS-3 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (3, 8, 8.00), 0.7381},
    {std::make_tuple (3, 8, 12.00), 0.3177},
    {std::make_tuple (3, 8, 16.00), 0.0805},
    {std::make_tuple (3, 8, 20.00), 0.0168},

    //MCS-4 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (4, 8, 11.00), 0.7842},
    {std::make_tuple (4, 8, 15.00), 0.4129},
    {std::make_tuple (4, 8, 19.00), 0.1398},
    {std::make_tuple (4, 8, 22.00), 0.0492},

    //MCS-5 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (5, 8, 14.00), 0.8343},
    {std::make_tuple (5, 8, 18.00), 0.4512},
    {std::make_tuple (5, 8, 22.00), 0.1427},
    {std::make_tuple (5, 8, 26.00), 0.0317},
    {std::make_tuple (5, 8, 28.00), 0.0159},

    //MCS-6 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (6, 8, 16.00), 0.8051},
    {std::make_tuple (6, 8, 20.00), 0.4113},
    {std::make_tuple (6, 8, 24.00), 0.1277},
    {std::make_tuple (6, 8, 28.00), 0.0334},

    //MCS-7 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (7, 8, 18.00), 0.7888},
    {std::make_tuple (7, 8, 22.00), 0.4173},
    {std::make_tuple (7, 8, 26.00), 0.1522},
    {std::make_tuple (7, 8, 30.00), 0.0530},

    //MCS-8 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (8, 8, 21.50), 0.7703},
    {std::make_tuple (8, 8, 25.50), 0.3631},
    {std::make_tuple (8, 8, 29.50), 0.1089},
    {std::make_tuple (8, 8, 33.50), 0.0266},
    {std::make_tuple (8, 8, 37.50), 0.0109},

    //MCS-9 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (9, 8, 20.00), 0.9528},
    {std::make_tuple (9, 8, 24.00), 0.7271},
    {std::make_tuple (9, 8, 28.00), 0.3467},
    {std::make_tuple (9, 8, 32.00), 0.1116},
    {std::make_tuple (9, 8, 36.00), 0.0360},

    //MCS-10 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (10, 8, 21.50), 0.9853},
    {std::make_tuple (10, 8, 25.50), 0.8427},
    {std::make_tuple (10, 8, 29.50), 0.4668},
    {std::make_tuple (10, 8, 33.50), 0.1400},
    {std::make_tuple (10, 8, 37.50), 0.0307},

    //MCS-11 8MHz (OFDMA - 106 subcarriers)
    {std::make_tuple (11, 8, 22.00), 0.9943},
    {std::make_tuple (11, 8, 26.00), 0.9346},
    {std::make_tuple (11, 8, 30.00), 0.6612},
    {std::make_tuple (11, 8, 34.00), 0.2835},
    {std::make_tuple (11, 8, 38.00), 0.0803},

    //MCS-0 20MHz
    {std::make_tuple (0, 20, -3.00), 0.9292},
    {std::make_tuple (0, 20, -2.00), 0.8746},
    {std::make_tuple (0, 20, -1.00), 0.7902},
    {std::make_tuple (0, 20, 0.00), 0.6639},
    {std::make_tuple (0, 20, 1.00), 0.5303},
    {std::make_tuple (0, 20, 2.00), 0.3990},
    {std::make_tuple (0, 20, 3.00), 0.2795},
    {std::make_tuple (0, 20, 4.00), 0.1847},
    {std::make_tuple (0, 20, 5.00), 0.1123},
    {std::make_tuple (0, 20, 6.00), 0.0645},
    {std::make_tuple (0, 20, 7.00), 0.0337},
    {std::make_tuple (0, 20, 8.00), 0.0159},
    {std::make_tuple (0, 20, 9.00), 0.0087},
    {std::make_tuple (0, 20, 10.00), 0.0057},

    //MCS-1 20MHz
    {std::make_tuple (1, 20, 0.00), 0.9345},
    {std::make_tuple (1, 20, 4.00), 0.5199},
    {std::make_tuple (1, 20, 8.00), 0.1034},
    {std::make_tuple (1, 20, 12.00), 0.0085},

    //MCS-2 20MHz
    {std::make_tuple (2, 20, 2.00), 0.9783},
    {std::make_tuple (2, 20, 6.00), 0.7431},
    {std::make_tuple (2, 20, 10.00), 0.3015},
    {std::make_tuple (2, 20, 14.00), 0.0756},
    {std::make_tuple (2, 20, 18.00), 0.0039},

    //MCS-3 20MHz
    {std::make_tuple (3, 20, 8.00), 0.7597},
    {std::make_tuple (3, 20, 12.00), 0.2659},
    {std::make_tuple (3, 20, 16.00), 0.0382},
    {std::make_tuple (3, 20, 20.00), 0.0018},

    //MCS-4 20MHz
    {std::make_tuple (4, 20, 11.00), 0.8420},
    {std::make_tuple (4, 20, 15.00), 0.4165},
    {std::make_tuple (4, 20, 19.00), 0.1051},
    {std::make_tuple (4, 20, 23.00), 0.0077},

    //MCS-5 20MHz
    {std::make_tuple (5, 20, 14.00), 0.8795},
    {std::make_tuple (5, 20, 18.00), 0.4341},
    {std::make_tuple (5, 20, 22.00), 0.0868},
    {std::make_tuple (5, 20, 26.00), 0.0054},
    {std::make_tuple (5, 20, 28.00), 0.0010},

    //MCS-6 20MHz
    {std::make_tuple (6, 20, 16.00), 0.8520},
    {std::make_tuple (6, 20, 20.00), 0.4028},
    {std::make_tuple (6, 20, 24.00), 0.0852},
    {std::make_tuple (6, 20, 28.00), 0.0054},

    //MCS-7 20MHz
    {std::make_tuple (7, 20, 18.00), 0.8109},
    {std::make_tuple (7, 20, 22.00), 0.3456},
    {std::make_tuple (7, 20, 26.00), 0.0717},
    {std::make_tuple (7, 20, 30.00), 0.0041},

    //MCS-8 20MHz
    {std::make_tuple (8, 20, 21.50), 0.8065},
    {std::make_tuple (8, 20, 25.50), 0.3360},
    {std::make_tuple (8, 20, 29.50), 0.0520},
    {std::make_tuple (8, 20, 33.50), 0.0040},

    //MCS-9 20MHz
    {std::make_tuple (9, 20, 20.00), 0.9770},
    {std::make_tuple (9, 20, 24.00), 0.7201},
    {std::make_tuple (9, 20, 28.00), 0.2334},
    {std::make_tuple (9, 20, 32.00), 0.0313},
    {std::make_tuple (9, 20, 36.00), 0.0073},

    //MCS-10 20MHz
    {std::make_tuple (10, 20, 21.50), 0.9938},
    {std::make_tuple (10, 20, 23.50), 0.9702},
    {std::make_tuple (10, 20, 25.50), 0.8828},
    {std::make_tuple (10, 20, 27.50), 0.6871},
    {std::make_tuple (10, 20, 29.50), 0.4336},
    {std::make_tuple (10, 20, 31.50), 0.2137},
    {std::make_tuple (10, 20, 32.00), 0.1745},
    {std::make_tuple (10, 20, 32.50), 0.1382},
    {std::make_tuple (10, 20, 33.00), 0.1083},
    {std::make_tuple (10, 20, 33.50), 0.0858},
    {std::make_tuple (10, 20, 34.00), 0.0631},
    {std::make_tuple (10, 20, 34.50), 0.0503},

    //MCS-11 20MHz
    {std::make_tuple (11, 20, 22.00), 0.9991},
    {std::make_tuple (11, 20, 26.00), 0.9612},
    {std::make_tuple (11, 20, 30.00), 0.6257},
    {std::make_tuple (11, 20, 34.00), 0.1717},
    {std::make_tuple (11, 20, 34.50), 0.1410},
    {std::make_tuple (11, 20, 35.00), 0.1056},
    {std::make_tuple (11, 20, 35.50), 0.0798},
    {std::make_tuple (11, 20, 36.00), 0.0659},
    {std::make_tuple (11, 20, 36.50), 0.0508},
    {std::make_tuple (11, 20, 37.00), 0.0368},
    {std::make_tuple (11, 20, 37.50), 0.0284},
    {std::make_tuple (11, 20, 38.00), 0.0235},

    //MCS-0 40MHz
    {std::make_tuple (0, 40, -1.50), 0.8987},
    {std::make_tuple (0, 40, 1.50), 0.4652},
    {std::make_tuple (0, 40, 3.00), 0.2342},
    {std::make_tuple (0, 40, 4.50), 0.0874},
    {std::make_tuple (0, 40, 6.00), 0.0248},
    {std::make_tuple (0, 40, 7.00), 0.0092},

    //MCS-1 40MHz
    {std::make_tuple (1, 40, 0.00), 0.9799},
    {std::make_tuple (1, 40, 4.00), 0.6185},
    {std::make_tuple (1, 40, 6.00), 0.2927},
    {std::make_tuple (1, 40, 8.00), 0.0933},
    {std::make_tuple (1, 40, 10.00), 0.0185},
    {std::make_tuple (1, 40, 12.00), 0.0035},

    //MCS-2 40MHz
    {std::make_tuple (2, 40, 2.00), 0.9964},
    {std::make_tuple (2, 40, 6.00), 0.8296},
    {std::make_tuple (2, 40, 10.00), 0.2761},
    {std::make_tuple (2, 40, 12.50), 0.0765},
    {std::make_tuple (2, 40, 13.00), 0.0588},
    {std::make_tuple (2, 40, 13.50), 0.0439},
    {std::make_tuple (2, 40, 14.00), 0.0314},
    {std::make_tuple (2, 40, 14.50), 0.0236},
    {std::make_tuple (2, 40, 15.00), 0.0187},
    {std::make_tuple (2, 40, 15.50), 0.0057},

    //MCS-3 40MHz
    {std::make_tuple (3, 40, 8.00), 0.8262},
    {std::make_tuple (3, 40, 12.00), 0.2106},
    {std::make_tuple (3, 40, 14.00), 0.0521},
    {std::make_tuple (3, 40, 14.50), 0.0375},
    {std::make_tuple (3, 40, 15.00), 0.0240},
    {std::make_tuple (3, 40, 15.50), 0.0169},
    {std::make_tuple (3, 40, 16.00), 0.0096},
    {std::make_tuple (3, 40, 16.50), 0.0062},
    {std::make_tuple (3, 40, 17.00), 0.0050},
    {std::make_tuple (3, 40, 17.50), 0.0027},
    {std::make_tuple (3, 40, 18.00), 0.0019},

    //MCS-4 40MHz
    {std::make_tuple (4, 40, 11.00), 0.9177},
    {std::make_tuple (4, 40, 15.00), 0.4255},
    {std::make_tuple (4, 40, 17.00), 0.1817},
    {std::make_tuple (4, 40, 19.00), 0.0627},
    {std::make_tuple (4, 40, 19.50), 0.0457},
    {std::make_tuple (4, 40, 20.00), 0.0340},
    {std::make_tuple (4, 40, 20.50), 0.0256},
    {std::make_tuple (4, 40, 21.00), 0.0212},

    //MCS-5 40MHz
    {std::make_tuple (5, 40, 14.00), 0.9378},
    {std::make_tuple (5, 40, 18.00), 0.4299},
    {std::make_tuple (5, 40, 22.00), 0.0375},
    {std::make_tuple (5, 40, 22.50), 0.0253},
    {std::make_tuple (5, 40, 23.00), 0.0196},
    {std::make_tuple (5, 40, 23.50), 0.0118},
    {std::make_tuple (5, 40, 24.00), 0.0092},
    {std::make_tuple (5, 40, 24.50), 0.0045},
    {std::make_tuple (5, 40, 25.00), 0.0025},

    //MCS-6 40MHz
    {std::make_tuple (6, 40, 16.00), 0.9168},
    {std::make_tuple (6, 40, 20.00), 0.3982},
    {std::make_tuple (6, 40, 22.00), 0.1508},
    {std::make_tuple (6, 40, 24.00), 0.0410},
    {std::make_tuple (6, 40, 26.00), 0.0113},
    {std::make_tuple (6, 40, 28.00), 0.0031},

    //MCS-7 40MHz
    {std::make_tuple (7, 40, 18.00), 0.8909},
    {std::make_tuple (7, 40, 22.00), 0.3484},
    {std::make_tuple (7, 40, 24.00), 0.1279},
    {std::make_tuple (7, 40, 26.00), 0.0358},
    {std::make_tuple (7, 40, 28.00), 0.0117},
    {std::make_tuple (7, 40, 30.00), 0.0040},

    //MCS-8 40MHz
    {std::make_tuple (8, 40, 21.50), 0.8729},
    {std::make_tuple (8, 40, 25.50), 0.2982},
    {std::make_tuple (8, 40, 28.00), 0.0704},
    {std::make_tuple (8, 40, 28.50), 0.0500},
    {std::make_tuple (8, 40, 29.00), 0.0339},
    {std::make_tuple (8, 40, 29.50), 0.0207},
    {std::make_tuple (8, 40, 30.00), 0.0148},
    {std::make_tuple (8, 40, 30.50), 0.0113},
    {std::make_tuple (8, 40, 31.00), 0.0075},

    //MCS-9 40MHz
    {std::make_tuple (9, 40, 20.00), 0.9966},
    {std::make_tuple (9, 40, 24.00), 0.8198},
    {std::make_tuple (9, 40, 28.00), 0.2323},
    {std::make_tuple (9, 40, 30.00), 0.0702},
    {std::make_tuple (9, 40, 30.50), 0.0456},
    {std::make_tuple (9, 40, 31.00), 0.0364},
    {std::make_tuple (9, 40, 31.50), 0.0255},
    {std::make_tuple (9, 40, 32.00), 0.0171},
    {std::make_tuple (9, 40, 32.50), 0.0117},
    {std::make_tuple (9, 40, 33.00), 0.0089},

    //MCS-10 40MHz
    {std::make_tuple (10, 40, 21.50), 0.9990},
    {std::make_tuple (10, 40, 23.50), 0.9868},
    {std::make_tuple (10, 40, 25.50), 0.9079},
    {std::make_tuple (10, 40, 27.50), 0.6645},
    {std::make_tuple (10, 40, 29.50), 0.3206},
    {std::make_tuple (10, 40, 31.50), 0.0944},
    {std::make_tuple (10, 40, 32.00), 0.0653},
    {std::make_tuple (10, 40, 32.50), 0.0415},
    {std::make_tuple (10, 40, 33.00), 0.0275},
    {std::make_tuple (10, 40, 33.50), 0.0158},
    {std::make_tuple (10, 40, 34.00), 0.0099},
    {std::make_tuple (10, 40, 34.50), 0.0054},

    //MCS-11 40MHz
    {std::make_tuple (11, 40, 22.00), 1.0000},
    {std::make_tuple (11, 40, 26.00), 0.9943},
    {std::make_tuple (11, 40, 30.00), 0.7714},
    {std::make_tuple (11, 40, 34.00), 0.2079},
    {std::make_tuple (11, 40, 34.50), 0.1565},
    {std::make_tuple (11, 40, 35.00), 0.1235},
    {std::make_tuple (11, 40, 35.50), 0.0901},
    {std::make_tuple (11, 40, 36.00), 0.0678},
    {std::make_tuple (11, 40, 36.50), 0.0478},
    {std::make_tuple (11, 40, 37.00), 0.0352},
    {std::make_tuple (11, 40, 37.50), 0.0266},
    {std::make_tuple (11, 40, 38.00), 0.0199},

    //MCS-0 80MHz
    {std::make_tuple (0, 80, -1.50), 0.9553},
    {std::make_tuple (0, 80, 1.50), 0.4318},
    {std::make_tuple (0, 80, 2.00), 0.3267},
    {std::make_tuple (0, 80, 2.50), 0.2280},
    {std::make_tuple (0, 80, 3.00), 0.1468},
    {std::make_tuple (0, 80, 3.50), 0.0930},
    {std::make_tuple (0, 80, 4.00), 0.0549},
    {std::make_tuple (0, 80, 4.50), 0.0309},
    {std::make_tuple (0, 80, 5.00), 0.0158},
    {std::make_tuple (0, 80, 5.50), 0.0078},

    //MCS-1 80MHz
    {std::make_tuple (1, 80, 0.00), 0.9951},
    {std::make_tuple (1, 80, 4.00), 0.5661},
    {std::make_tuple (1, 80, 6.00), 0.1495},
    {std::make_tuple (1, 80, 6.50), 0.1005},
    {std::make_tuple (1, 80, 7.00), 0.0578},
    {std::make_tuple (1, 80, 7.50), 0.0320},
    {std::make_tuple (1, 80, 8.00), 0.0177},
    {std::make_tuple (1, 80, 8.50), 0.0076},
    {std::make_tuple (1, 80, 9.00), 0.0036},

    //MCS-2 80MHz
    {std::make_tuple (2, 80, 2.00), 0.9998},
    {std::make_tuple (2, 80, 6.00), 0.9165},
    {std::make_tuple (2, 80, 10.00), 0.2389},
    {std::make_tuple (2, 80, 11.00), 0.1158},
    {std::make_tuple (2, 80, 11.50), 0.0767},
    {std::make_tuple (2, 80, 12.00), 0.0509},
    {std::make_tuple (2, 80, 12.50), 0.0302},
    {std::make_tuple (2, 80, 13.00), 0.0167},
    {std::make_tuple (2, 80, 13.50), 0.0118},
    {std::make_tuple (2, 80, 14.00), 0.0064},

    //MCS-3 80MHz
    {std::make_tuple (3, 80, 8.00), 0.8956},
    {std::make_tuple (3, 80, 10.00), 0.5248},
    {std::make_tuple (3, 80, 11.00), 0.3054},
    {std::make_tuple (3, 80, 12.00), 0.1419},
    {std::make_tuple (3, 80, 12.50), 0.0870},
    {std::make_tuple (3, 80, 13.00), 0.0550},
    {std::make_tuple (3, 80, 13.50), 0.0307},
    {std::make_tuple (3, 80, 14.00), 0.0160},
    {std::make_tuple (3, 80, 14.50), 0.0092},

    //MCS-4 80MHz
    {std::make_tuple (4, 80, 11.00), 0.9750},
    {std::make_tuple (4, 80, 15.00), 0.4394},
    {std::make_tuple (4, 80, 16.00), 0.2606},
    {std::make_tuple (4, 80, 17.00), 0.1285},
    {std::make_tuple (4, 80, 17.50), 0.0842},
    {std::make_tuple (4, 80, 18.00), 0.0553},
    {std::make_tuple (4, 80, 18.50), 0.0356},
    {std::make_tuple (4, 80, 19.00), 0.0216},
    {std::make_tuple (4, 80, 19.50), 0.0120},
    {std::make_tuple (4, 80, 20.00), 0.0083},

    //MCS-5 80MHz
    {std::make_tuple (5, 80, 14.00), 0.9820},
    {std::make_tuple (5, 80, 18.00), 0.4208},
    {std::make_tuple (5, 80, 19.00), 0.2331},
    {std::make_tuple (5, 80, 19.50), 0.1567},
    {std::make_tuple (5, 80, 20.00), 0.1006},
    {std::make_tuple (5, 80, 20.50), 0.0633},
    {std::make_tuple (5, 80, 21.00), 0.0392},
    {std::make_tuple (5, 80, 21.50), 0.0201},
    {std::make_tuple (5, 80, 22.00), 0.0108},

    //MCS-6 80MHz
    {std::make_tuple (6, 80, 16.00), 0.9582},
    {std::make_tuple (6, 80, 20.00), 0.2696},
    {std::make_tuple (6, 80, 20.50), 0.1889},
    {std::make_tuple (6, 80, 21.00), 0.1224},
    {std::make_tuple (6, 80, 21.50), 0.0808},
    {std::make_tuple (6, 80, 22.00), 0.0480},
    {std::make_tuple (6, 80, 22.50), 0.0270},
    {std::make_tuple (6, 80, 23.00), 0.0140},
    {std::make_tuple (6, 80, 23.50), 0.0072},
    {std::make_tuple (6, 80, 24.00), 0.0039},

    //MCS-7 80MHz
    {std::make_tuple (7, 80, 18.00), 0.9656},
    {std::make_tuple (7, 80, 22.00), 0.3738},
    {std::make_tuple (7, 80, 23.00), 0.2059},
    {std::make_tuple (7, 80, 23.50), 0.1394},
    {std::make_tuple (7, 80, 24.00), 0.0996},
    {std::make_tuple (7, 80, 24.50), 0.0614},
    {std::make_tuple (7, 80, 25.00), 0.0356},
    {std::make_tuple (7, 80, 25.50), 0.0234},
    {std::make_tuple (7, 80, 26.00), 0.0136},
    {std::make_tuple (7, 80, 26.50), 0.0078},

    //MCS-8 80MHz
    {std::make_tuple (8, 80, 21.50), 0.9025},
    {std::make_tuple (8, 80, 23.00), 0.6573},
    {std::make_tuple (8, 80, 24.50), 0.3124},
    {std::make_tuple (8, 80, 25.50), 0.1465},
    {std::make_tuple (8, 80, 26.00), 0.0960},
    {std::make_tuple (8, 80, 26.50), 0.0558},
    {std::make_tuple (8, 80, 27.00), 0.0324},
    {std::make_tuple (8, 80, 27.50), 0.0173},

    //MCS-9 80MHz
    {std::make_tuple (9, 80, 20.00), 0.9997},
    {std::make_tuple (9, 80, 24.00), 0.8819},
    {std::make_tuple (9, 80, 26.00), 0.5224},
    {std::make_tuple (9, 80, 28.00), 0.1573},
    {std::make_tuple (9, 80, 28.50), 0.1009},
    {std::make_tuple (9, 80, 29.00), 0.0622},
    {std::make_tuple (9, 80, 29.50), 0.0391},
    {std::make_tuple (9, 80, 30.00), 0.0217},
    {std::make_tuple (9, 80, 30.50), 0.0118},

    //MCS-10 80MHz
    {std::make_tuple (10, 80, 21.50), 1.0000},
    {std::make_tuple (10, 80, 23.50), 0.9989},
    {std::make_tuple (10, 80, 25.50), 0.9727},
    {std::make_tuple (10, 80, 27.50), 0.7704},
    {std::make_tuple (10, 80, 29.50), 0.3240},
    {std::make_tuple (10, 80, 31.50), 0.0616},
    {std::make_tuple (10, 80, 32.00), 0.0359},
    {std::make_tuple (10, 80, 32.50), 0.0193},
    {std::make_tuple (10, 80, 33.00), 0.0100},
    {std::make_tuple (10, 80, 33.50), 0.0048},
    {std::make_tuple (10, 80, 34.00), 0.0022},
    {std::make_tuple (10, 80, 34.50), 0.0012},

    //MCS-11 80MHz
    {std::make_tuple (11, 80, 22.00), 1.0000},
    {std::make_tuple (11, 80, 26.00), 0.9990},
    {std::make_tuple (11, 80, 30.00), 0.7407},
    {std::make_tuple (11, 80, 34.00), 0.0631},
    {std::make_tuple (11, 80, 34.50), 0.0356},
    {std::make_tuple (11, 80, 35.00), 0.0218},
    {std::make_tuple (11, 80, 35.50), 0.0120},
    {std::make_tuple (11, 80, 36.00), 0.0062},
    {std::make_tuple (11, 80, 36.50), 0.0024},
    {std::make_tuple (11, 80, 37.00), 0.0013},
    {std::make_tuple (11, 80, 37.50), 0.0005},
    {std::make_tuple (11, 80, 38.00), 0.0003},

    //MCS-0 160MHz
    {std::make_tuple (0, 160, -1.50), 0.9907},
    {std::make_tuple (0, 160, 1.50), 0.4353},
    {std::make_tuple (0, 160, 2.00), 0.2917},
    {std::make_tuple (0, 160, 2.50), 0.1685},
    {std::make_tuple (0, 160, 3.50), 0.0430},
    {std::make_tuple (0, 160, 4.00), 0.0177},
    {std::make_tuple (0, 160, 5.00), 0.0027},
    {std::make_tuple (0, 160, 5.50), 0.0007},

    //MCS-1 160MHz
    {std::make_tuple (1, 160, 0.00), 0.9996},
    {std::make_tuple (1, 160, 4.00), 0.5772},
    {std::make_tuple (1, 160, 6.00), 0.0825},
    {std::make_tuple (1, 160, 6.50), 0.0365},
    {std::make_tuple (1, 160, 7.00), 0.0167},
    {std::make_tuple (1, 160, 7.50), 0.0057},
    {std::make_tuple (1, 160, 8.50), 0.0006},
    {std::make_tuple (1, 160, 9.00), 0.0001},

    //MCS-2 80MHz
    {std::make_tuple (2, 160, 6.00), 0.9715},
    {std::make_tuple (2, 160, 10.00), 0.1926},
    {std::make_tuple (2, 160, 11.00), 0.0627},
    {std::make_tuple (2, 160, 11.50), 0.0300},
    {std::make_tuple (2, 160, 12.00), 0.0164},
    {std::make_tuple (2, 160, 12.50), 0.0065},
    {std::make_tuple (2, 160, 13.00), 0.0030},
    {std::make_tuple (2, 160, 13.50), 0.0014},

    //MCS-3 160MHz
    {std::make_tuple (3, 160, 8.00), 0.9661},
    {std::make_tuple (3, 160, 10.00), 0.6003},
    {std::make_tuple (3, 160, 11.00), 0.3063},
    {std::make_tuple (3, 160, 12.00), 0.1064},
    {std::make_tuple (3, 160, 13.00), 0.0270},
    {std::make_tuple (3, 160, 13.50), 0.0099},
    {std::make_tuple (3, 160, 14.00), 0.0040},
    {std::make_tuple (3, 160, 14.50), 0.0015},

    //MCS-4 160MHz
    {std::make_tuple (4, 160, 11.00), 0.9898},
    {std::make_tuple (4, 160, 15.00), 0.2665},
    {std::make_tuple (4, 160, 17.00), 0.0229},
    {std::make_tuple (4, 160, 18.00), 0.0037},
    {std::make_tuple (4, 160, 18.50), 0.0013},
    {std::make_tuple (4, 160, 19.00), 0.0005},
    {std::make_tuple (4, 160, 19.50), 0.0002},
    {std::make_tuple (4, 160, 20.00), 0.0001},

    //MCS-5 160MHz
    {std::make_tuple (5, 160, 14.00), 0.9982},
    {std::make_tuple (5, 160, 18.00), 0.4384},
    {std::make_tuple (5, 160, 20.00), 0.0561},
    {std::make_tuple (5, 160, 20.50), 0.0255},
    {std::make_tuple (5, 160, 21.00), 0.0104},
    {std::make_tuple (5, 160, 21.50), 0.0043},
    {std::make_tuple (5, 160, 22.00), 0.0015},
    {std::make_tuple (5, 160, 22.50), 0.0007},

    //MCS-6 160MHz
    {std::make_tuple (6, 160, 16.00), 0.9888},
    {std::make_tuple (6, 160, 19.00), 0.4855},
    {std::make_tuple (6, 160, 20.50), 0.1284},
    {std::make_tuple (6, 160, 21.00), 0.0692},
    {std::make_tuple (6, 160, 21.50), 0.0306},
    {std::make_tuple (6, 160, 22.50), 0.0060},
    {std::make_tuple (6, 160, 23.00), 0.0022},
    {std::make_tuple (6, 160, 23.50), 0.0007},

    //MCS-7 160MHz
    {std::make_tuple (7, 160, 18.00), 0.9949},
    {std::make_tuple (7, 160, 22.00), 0.3593},
    {std::make_tuple (7, 160, 23.50), 0.0883},
    {std::make_tuple (7, 160, 24.50), 0.0243},
    {std::make_tuple (7, 160, 25.00), 0.0120},
    {std::make_tuple (7, 160, 25.50), 0.0054},
    {std::make_tuple (7, 160, 26.00), 0.0018},
    {std::make_tuple (7, 160, 26.50), 0.0007},

    //MCS-8 160MHz
    {std::make_tuple (8, 160, 21.00), 0.9893},
    {std::make_tuple (8, 160, 24.00), 0.4533},
    {std::make_tuple (8, 160, 26.00), 0.0557},
    {std::make_tuple (8, 160, 26.50), 0.0262},
    {std::make_tuple (8, 160, 27.00), 0.0108},
    {std::make_tuple (8, 160, 27.50), 0.0047},
    {std::make_tuple (8, 160, 28.00), 0.0016},
    {std::make_tuple (8, 160, 28.50), 0.0005},

    //MCS-9 160MHz
    {std::make_tuple (9, 160, 22.00), 0.9994},
    {std::make_tuple (9, 160, 26.00), 0.6278},
    {std::make_tuple (9, 160, 28.50), 0.0757},
    {std::make_tuple (9, 160, 29.00), 0.0405},
    {std::make_tuple (9, 160, 29.50), 0.0192},
    {std::make_tuple (9, 160, 30.00), 0.0082},
    {std::make_tuple (9, 160, 30.50), 0.0034},
    {std::make_tuple (9, 160, 31.00), 0.0013},

    //MCS-10 160MHz
    {std::make_tuple (10, 160, 21.50), 1.0000},
    {std::make_tuple (10, 160, 23.50), 1.0000},
    {std::make_tuple (10, 160, 25.50), 0.9993},
    {std::make_tuple (10, 160, 27.50), 0.9288},
    {std::make_tuple (10, 160, 29.50), 0.4876},
    {std::make_tuple (10, 160, 31.50), 0.0763},
    {std::make_tuple (10, 160, 32.00), 0.0382},
    {std::make_tuple (10, 160, 32.50), 0.0167},
    {std::make_tuple (10, 160, 33.00), 0.0075},
    {std::make_tuple (10, 160, 33.50), 0.0028},
    {std::make_tuple (10, 160, 34.00), 0.0009},
    {std::make_tuple (10, 160, 34.50), 0.0004},

    //MCS-11 160MHz
    {std::make_tuple (11, 160, 22.00), 1.0000},
    {std::make_tuple (11, 160, 26.00), 1.0000},
    {std::make_tuple (11, 160, 30.00), 0.8148},
    {std::make_tuple (11, 160, 34.00), 0.0242},
    {std::make_tuple (11, 160, 34.50), 0.0101},
    {std::make_tuple (11, 160, 35.00), 0.0041},
    {std::make_tuple (11, 160, 35.50), 0.0013},
    {std::make_tuple (11, 160, 36.00), 0.0005},
};

static const LogSgnMatlabResults logSgnMixtureSisoResults =
{
    //MCS-0 20MHz
    {std::make_tuple (0, 20, -3.00), 0.9292},
    {std::make_tuple (0, 20, -2.00), 0.8398},
    {std::make_tuple (0, 20, -1.00), 0.7519},
    {std::make_tuple (0, 20, 0.00), 0.6639},
    {std::make_tuple (0, 20, 1.00), 0.5260},
    {std::make_tuple (0, 20, 2.00), 0.4076},
    {std::make_tuple (0, 20, 3.00), 0.2795},
    {std::make_tuple (0, 20, 4.00), 0.2112},
    {std::make_tuple (0, 20, 5.00), 0.1361},
    {std::make_tuple (0, 20, 6.00), 0.0645},
    {std::make_tuple (0, 20, 7.00), 0.0460},
    {std::make_tuple (0, 20, 8.00), 0.0280},
    {std::make_tuple (0, 20, 9.00), 0.0087},
    {std::make_tuple (0, 20, 10.00), 0.0057},
};

} //namespace ns3

#endif /* LOG_SGN_MATLAB_RESULTS_H */
