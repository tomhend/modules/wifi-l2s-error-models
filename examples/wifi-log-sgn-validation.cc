/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Sébastien Deronne <sebastien.deronne@gmail.com>
 *          Sian Jin <sianjin@uw.edu>
 */

// This example is used to validate Log-SGN results obtained in ns-3 against
// Matlab Log-SGN implementation and Matlab full PHY implementation.

#include <fstream>
#include <cmath>
#include <map>
#include <vector>
#include "ns3/gnuplot.h"
#include "ns3/command-line.h"
#include "ns3/rng-seed-manager.h"
#include "ns3/log-sgn-error-rate-model.h"
#include "ns3/wifi-tx-vector.h"
#include "ns3/log-sgn-matlab-results.h"

using namespace ns3;

int main (int argc, char *argv[])
{
  uint32_t size = 1000 * 8; //bits
  uint8_t mcs = 9;
  uint16_t bw = 40;
  bool mimo = false; // MIMO not yet supported
#ifdef NOTYET
  std::size_t mimoUsers = 1;
#endif
  uint8_t nMimoTx = 4;
  uint8_t nMimoRx = 2;
  uint8_t nMimoSs = 2;
  std::string format ("He");
  uint64_t iterations = 40000;

  CommandLine cmd (__FILE__);
  cmd.AddValue ("frameFormat", "The frame format to use: Ht, Vht or He", format);
  cmd.AddValue ("mcs", "The MCS to test", mcs);
  cmd.AddValue ("bw", "The channel width to test (in MHz)", bw);
#ifdef NOTYET
  cmd.AddValue ("mimo", "Enable MIMO if set, SISO is used otherwise", mimo);
  cmd.AddValue ("mimoUsers", "The number of simulataneous MIMO users (if more than 1 and MIMO is enabled, then MU-MIMO is used)", mimoUsers);
  cmd.AddValue ("nMimoTx", "The number of TX antennas used for MIMO", nMimoTx);
  cmd.AddValue ("nMimoRx", "The number of RX antennas used for MIMO", nMimoRx);
  cmd.AddValue ("nMimoSs", "The number of spatial streams used for MIMO", nMimoSs);
#endif
  cmd.AddValue ("iterations", "The number of iterations to run", iterations);
  cmd.Parse (argc, argv);

  uint8_t rxAntennas = mimo ? nMimoRx : 1;
  uint8_t txAntennas = mimo ? nMimoTx : 1;
  uint8_t spatialStreams = mimo ? nMimoSs : 1;
  if (format == "Ht")
    {
      spatialStreams = 1 + (mcs / 8);
    }

  Ptr <LogSgnErrorRateModel> sgn = CreateObject<LogSgnErrorRateModel> ();

  std::stringstream mode;
  mode << format << "Mcs" << +mcs;

  WifiTxVector txVector;
  txVector.SetMode (WifiMode (mode.str ()));
  txVector.SetChannelWidth (bw);
  txVector.SetNTx (txAntennas);
  txVector.SetNss (spatialStreams);
  txVector.SetLdpc (true);

  bool muMimo = false;
  std::cout << mode.str () << " "
            << txVector.GetChannelWidth () << " MHz "
            << +txAntennas << "x"
            << +rxAntennas << ":"
            << +spatialStreams << std::endl;

  std::stringstream title;
  title << "wifi-log-sgn-validation-mcs" << +mcs << "-bw" << bw << "-ldpc-";
  title << "siso";
  std::ofstream errormodelfile (title.str () + ".plt");
  Gnuplot plot = Gnuplot (title.str () + ".eps");

  Gnuplot2dDataset fullPhyDataset (mode.str ());
  Gnuplot2dDataset logSgnDataset (mode.str ());
  Gnuplot2dDataset ns3Dataset (mode.str ());

  LogSgnMatlabResults fullPhyResults = fullPhySisoResults;
  LogSgnMatlabResults logSgnResults = logSgnSisoResults;
  auto itFullPhy = fullPhyResults.begin ();
  auto itLogSgn = logSgnResults.begin ();
  double firstSnr, lastSnr;
  bool firstSet = false;
  for (; itFullPhy != fullPhyResults.end () && itLogSgn != logSgnResults.end (); )
    {
      if ((std::get<0> (itFullPhy->first) != mcs) || (std::get<1> (itFullPhy->first) != bw))
        {
          ++itFullPhy;
          continue;
        }
      if ((std::get<0> (itLogSgn->first) != mcs) || (std::get<1> (itLogSgn->first) != bw))
        {
          ++itLogSgn;
          continue;
        }
      if (itFullPhy->first != itLogSgn->first)
        {
          break;
        }
      double snr = std::get<2> (itFullPhy->first);
      if (!firstSet)
        {
          firstSnr = snr;
          firstSet = true;
        }
      lastSnr = snr;

      RngSeedManager::SetSeed (1);
      RngSeedManager::SetRun (1);
      int64_t streamNumber = 10;
      sgn->AssignStreams (streamNumber);

      double perAverage = 0;
      for (uint64_t runNumber = 0; runNumber < iterations; runNumber++)
        {
          perAverage += (1 - sgn->GetChunkSuccessRate (WifiMode (mode.str ()), txVector, std::pow (10.0, snr / 10.0), size, rxAntennas, WIFI_PPDU_FIELD_DATA, muMimo ? 0 : SU_STA_ID));
        }
      double per = (perAverage / iterations);
      if (per < 0 || per > 1)
        {
          //error
          exit (1);
        }
      std::cout << "PER for SNR=" << snr << "dB with " << iterations << " run is " << per << std::endl;

      ns3Dataset.Add (snr, per);
      logSgnDataset.Add (snr, itLogSgn->second);
      fullPhyDataset.Add (snr, itFullPhy->second);

      ++itFullPhy;
      ++itLogSgn;
    }

  if (!firstSet)
    {
      NS_FATAL_ERROR ("combination MCS " << +mcs << " and BW " << bw << " not available in one or both table(s)");
    }

  ns3Dataset.SetTitle ("ns-3 Log-SGN");
  plot.AddDataset (ns3Dataset);

  logSgnDataset.SetTitle ("Matlab Log-SGN");
  plot.AddDataset (logSgnDataset);

  fullPhyDataset.SetTitle ("Matlab full PHY");
  plot.AddDataset (fullPhyDataset);

  plot.SetTerminal ("postscript eps color enh \"Times-BoldItalic\"");
  plot.SetLegend ("RX SNR (dB)", "Average PER");

  std::stringstream plotExtra;
  plotExtra << "set xrange [" << firstSnr << ":" << lastSnr << "]\n\
set log y\n\
set yrange [0.001:1]\n";

  plotExtra << "set style line 1 linewidth 5 linecolor rgb \"red\" \n";
  plotExtra << "set style line 2 linewidth 5 linecolor rgb \"blue\" \n";
  plotExtra << "set style line 3 linewidth 5 linecolor rgb \"green\" \n";

  plotExtra << "set style increment user";
  plot.SetExtra (plotExtra.str ());

  plot.GenerateOutput (errormodelfile);
  errormodelfile.close ();
}
