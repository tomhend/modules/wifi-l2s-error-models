import sys, os

sys.path.insert(0, os.path.abspath('extensions'))

extensions = ['sphinx.ext.autodoc', 'sphinx.ext.doctest', 'sphinx.ext.todo',
                    'sphinx.ext.coverage', 'sphinx.ext.imgmath', 'sphinx.ext.ifconfig',
                                  'sphinx.ext.autodoc']

latex_engine = 'xelatex'
todo_include_todos = True
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'wifi-l2s-error-models'
exclude_patterns = []
add_function_parentheses = True
#add_module_names = True
#modindex_common_prefix = []

project = u'WiFi Link-to-System Error Models'
copyright = u'2022'

version = '1.0'
release = '1.0'

latex_documents = [
  ('wifi-l2s-error-models', 'wifi-l2s-error-models.tex', u'Wi-Fi Link-to-System Error Models', u'University of Washington', 'manual'),
]

