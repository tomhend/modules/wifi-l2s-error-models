Wifi Link-to-System Error Models
--------------------------------

.. highlight:: cpp
.. |ns3| replace:: *ns-3*

.. heading hierarchy:
   ------------- Chapter
   ************* Section (#.#)
   ============= Subsection (#.#.#)
   ############# Paragraph (no number)

Model Description
*****************

This module contains the LogSgnErrorRateModel implementation for ns-3 wifi
models, described in [Jin20]_ and [Jin21a]_.  The tables implemented herein
were derived from link simulation results of OFDM Wi-Fi channels, and enable
more accurate packet error ratio (PER) modeling for selected multipath channels.

Three configurations for a specific multipath model (IEEE TGn channel model
D [Erc04]_) and a SISO antenna configuration are presently supported, but a
documented methodology exists to allow users to extend these tables to
other channels and configurations.

Design
======

Error models for packet-level wireless network simulation typically use
an abstraction of the channel, and other configuration details, to calculate a
received signal-to-noise ratio (SNR) for each packet.  Packet error ratio
(PER) curves or tables are used to convert received SNR to a PER, and then
a random variable is used to draw a random variate and compare the value
against the corresponding PER to determine if a packet is in error or is
received correctly.

In the |ns3| wifi module, existing error tables are for additive white
gaussian noise channels (AWGN) only.  Although the propagation models
can configure path loss and shadowing loss to affect the received SNR,
the received signal is still an attenuated AWGN signal with no
frequency-selective fading.  However, in practice, frequency-selective
fading arises due to time-dispersive (multipath) environments.  Therefore,
existing AWGN error models are not directly usable for multipath channels.

Link simulations such as MATLAB(TM) Wireless LAN Toolbox can produce
detailed physical layer simulations (also known as 'link simulations') to
determine PER curves for multipath
channels, but such physical layer simulators cannot be directly integrated
to a packet-level simulator such as |ns3| without incurring slow
execution times.  Therefore, a common practice in the field is to run
link simulations offline and to map the results to the existing AWGN-based
PER curves.  This is known as link-to-system mapping.  A popular technique
is known as Exponential Effective Signal to Noise Ratio Mapping (EESM).
It consists of mapping the signal into multiple subcarriers, generating
a fading channel instance, modifying the power of each subcarrier by a
different amount corresponding to the channel instance, and then compressing
the received vector of subcarrier SNRs into a quantity known as the
'effective SNR', which can then be used with the AWGN tables to compute
the PER.

As explained in [Jin20]_ and [Jin21a]_, the EESM process is still expensive
to compute, especially for MIMO channels.  This motivated the development
of the 'log-SGN' approach described in those publications.  Log-SGN works
by storing the coefficients of a skewed log-Gaussian distribution that
can be used with a random variable to directly draw a random
'effective SNR' that is used with the AWGN error rate table to find
a corresponding PER.  By doing so, the channel generation and related
computation can be avoided, at the expense of storing the parameters
of the log-SGN distribution.

|ns3| has a modular implementation for the Wi-Fi error models, and in the
past, three separate models have been used:

* ``TableBasedErrorRateModel`` (current |ns3| default)
* ``YansErrorRateModel``
* ``NistErrorRateModel``

The ``LogSgnErrorRateModel`` can be used instead of the above-listed models,
for OFDM-based Wi-Fi simulations.

Scope and Limitations
=====================

The error model data is based on link simulations of a specific configuration;
therefore, the ns-3 scenario must be sufficiently aligned with the link
simulation configuration for model accuracy.

Users may want to check the `Matlab code <https://github.com/sianjin/EESM-log-SGN>`_ for specifics, but in general, the following scope applies:

* IEEE TGn Channel Model D (indoor typical office model)
* Ideal physical layer implementation
* Ideal channel estimation
* No mobility/Doppler
* SISO (1 output and 1 input antenna)
* LDPC coding
* Interference-free

The code does not enforce that the |ns3| configuration matches the error
model assumptions; this is left to the simulation user.

Usage
*****

To use the LogSgnErrorRateModel, specify ``ns3::LogSgnErrorRateModel`` as
the ErrorModel for the SpectrumWifiPhy instance or helper; e.g.:

::

    SpectrumWifiPhyHelper spectrumPhy;
    spectrumPhy.SetErrorRateModel ("ns3::LogSgnErrorRateModel");

The ``wifi-l2s-error-models`` module must be linked by the ns-3 executable,
or else the program must be in the `scratch` directory.

At present, there is an attribute ``ChannelModelType`` to specify the channel
model type, but only one value (channel model D) is supported by the table data.

Examples
========

An example program comparing the output of the error model with reference
data from MATLAB(TM) is provided in ``examples/wifi-log-sgn-validation``.
This program will, for a selected frame format, MCS value, and channel
width, use the error model for `iterations` random selections for a
range of input SNRs and compare the values for the |ns3| model with
those of an independent implementation of log-SGN within MATLAB(TM),
as well as comparing with the original values used to derive the log-SGN
parameters (labelled `full PHY` in the plots).  A gnuplot file is
generated as the output of this example.

Validation
**********

The model has been validated by comparing the output of the |ns3| log-SGN
model with the original MATLAB(TM) data and ensuring that the values are
within a certain tolerance.  The test suite is implemented
in ``test/wifi-log-sgn-per-test.cc``.

.. [Jin20] Sian Jin, Sumit Roy, Weihua Jiang, and Thomas R. Henderson.  Efficient Abstractions for Implementing TGn Channel and OFDM-MIMO Links in ns-3.  In Proceedings of the 2020 Workshop on ns-3 (WNS3 2020).  Association for Computing Machinery, New York, NY, USA, 33–40.

.. [Jin21a] Sian Jin, Sumit Roy and Thomas R. Henderson. Efficient PHY Layer Abstraction for Fast Simulations in Complex System Environments.  In IEEE Transactions on Communications, vol. 69, no. 8, pp. 5649-5660, Aug. 2021.

.. [Erc04] V. Erceg. IEEE 802.11 Wireless LANs: TGn Channel Models, 2004.

